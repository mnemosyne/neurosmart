# LICENCE ET COPYRIGHT

Neurosmart est un logiciel et une ressource libre et ouverte, les fichiers originaux sont sous licence:

* [CeCILL-C] (https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) pour le code.
* [CC-BY] (https://creativecommons.org/licenses/by/3.0/fr/) pour le contenu.

Une utilisation commerciale et non commerciale est ainsi autorisée conformément à ces licences, pour ces fichiers, alors que les éléments tiers doivent être utilisés sous leurs licences spécifiques.

# LICENSE AND COPYRIGHT

Neurosmart is free software and resource, original files are licensed under:

* [CeCILL-C](https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) for the code.
* [CC-BY](https://creativecommons.org/licenses/by/3.0/fr/) for the contents.

Commercial and non-commercial use are thus permitted in compliance with these licenses, for these files, whereas third-party elements must be used under their related licenses.
