# Neurosmart

Comment marche notre cerveau ? On propose de découvrir les fonctions cérébrales à l’origine de nos comportements sensori-moteurs et cognitifs vitaux (comportement instinctif et motivé, sélection de l’action incarnée, prise de décision émotionnelle ou non, siège de la conscience de soi, …) en utilisant un outil logiciel permettant de dérouler des petits scénarios démonstratifs 

[![Presentation](./public/assets/doc/img/example-1.png)](https://youtu.be/AsGijFW9P3g)

[Présentation avec un exemple](https://youtu.be/2i7ut6yFNHs)

Pour aller plus loin, un document détaillé de [présentation du projet](https://hal.inria.fr/hal-03013647v1) et l'article ["Neurosmart, une histoire de cerveau et de passionnés"](http://www.scilogs.fr/intelligence-mecanique/neurosmart-une-histoire-de-cerveau-et-de-passionnes/).

## Pour qui est cette ressource ?

1. Le large public à qui on peut montrer des scénarios, mais aussi co-construire des ressources pluri-média pour partager des méthodes et connaissances (démarche de médiation scientifique participative) et permettre de débattre de ces sujets.
2. Les médiatrices et médiateurs scientifiques (chercheur·e·s et au delà) qui souhaitent co-construire de telles ressources, voir présenter des résultats de recherche impliquant l’animation d’une anatomie du système nerveux.
3. Les autrices ou auteurs de code informatique qui voudront réutiliser les technologies partagées pour dériver d’autres applications, en particulier se familiariser avec les langages de spécification (ici [JSON](https://www.json.org/json-fr.html) et [markdown](https://fr.wikipedia.org/wiki/Markdown)).

## De quoi parle t-elle ?

Le cerveau : c’est l’objet le plus complexe de l’univers que la science cherche à étudier. Mais comment marche notre cerveau ?
Venez découvrir ou mieux comprendre les fonctions cérébrales à l’origine de nos comportements sensori-moteurs et cognitifs vitaux (comme l’attention, la reconnaissance, la planification, la décision….) à travers leurs modèles mathématiques et informatiques : ce sont les neurosciences computationnelles.

_Comment fonctionne les mécanismes neuronaux de nos envies, de nos peurs, et plus largement de nos perceptions et de nos actions ? Où sont dans notre cerveau les aires du langage, de la vision ou liées au phénomène de conscience ?_

Voici quelques réponses visuelles, à partager avec toutes et tous. À l’heure où on parle beaucoup de neuro-ceci ou celà (neuro-économie, neuro-marketing, neuro-éducation¸ …) commençons par comprendre mieux ce que nous savons et ne savons pas, pour se faire une idée claire et critique de ces sujets.

## Comment visualiser cette ressource ?

La ressource est disponible ici  https://neurosmart.inria.fr (qui rebondit sur https://mnemosyne.gitlabpages.inria.fr/neurosmart )

Sélectionnez un scénario et suivez pas à pas les explications en visualisant les zones du cerveau activées.

## Comment créer ses propres ressources ?

0. [Télécharger les fichiers](./public/assets/doc/telecharger.md)
1. [Lancer un scénario](./public/assets/doc/lancer-scenario.md)
2. [Installer un nouveau scénario](./public/assets/doc/creer-scenario.md)
3. [Programmer son scénario](./public/assets/doc/faire-scenario.md)
4. [Tester son scénario](./public/assets/doc/tester-scenario.md)
5. [Déployer son scénario](./public/assets/doc/deployer-scenario.md)

## Comment contribuer à cette ressource ?

Il suffit demander à [rejoindre le projet](mailto:neurosmart_contact@inria.fr?subject=Je%20souhaite%20rejoindre%20le%20projet%20neurosmart&body=Bonjour,%0A%0C%20-%20Voici%20qui%20je%20suis%20:%20%0A%0C%20-%20Voici%20pourquoi%20je%20souhaite%20me%20joindre%20à%20vous%20:%20%0A%0CBien%20Cordialement.), nous vous fournirons toutes les informations.

Le code est [libre](LICENSE.md) et [documenté](https://mnemosyne.gitlabpages.inria.fr/neurosmart/assets/doc/).

Notre [pad de travail](https://pad.inria.fr/p/np_jtuFoFIoMBRZdkgm_Neurosmart-feedbacks) est ici.

## Qui sommes nous ?

Le projet [Neurosmart](https://gitlab.inria.fr/mnemosyne/neurosmart) est une co-création de l’équipe Inria [mnemosyne](https://www.inria.fr/equipes/mnemosyne) et [pixees](https://pixees.fr/neurosmart).

* Le code a été réalisé par Denis Chiron avec le conseil de Nicolas Rougier.
* Les collègues et partenaires Martine Courbin, Nicolas Rougier, Thierry Viéville et avec le conseil et les contributions de Benjamin Ninassi, Frédéric Alexandre, Ikram Chraibi Kaadoud, Sophie de Quatrebarbes, Thalita Firmot Drumond, Xavier Hinault, ont contribué.
* Le projet a été réalisé avec le support financier de la Fondation Blaise Pascal, du Réseau néo-aquitain de la culture scientifique, et de la mission Inria de Médiation Scientifique.
* Un avant-projet qui a permis d'étudier la faisabilité a été realisé par Netty Larisse, Jordane Barret, Yanis Mounsamy, et Tan Toan Nguyen de l'Université de Bordeaux.

Le projet a été soutenu par la [Fondation Blaise Pascal (FBP)](https://www.fondation-blaise-pascal.org ) et [Echo Science de Nouvelle Aquitaine (ES)](https://echosciences.nouvelle-aquitaine.science).	

Tous les contenus sont sous licence [CeCILL-C](http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html) pour les éléments logiciels et [CC-BY](https://creativecommons.org/licenses/by/2.0/fr/) pour les autres ressources.

