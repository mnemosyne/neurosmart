# Ajouter ses propres actions pour enrichir le mécanisme

Les actions permettent, au fil du scenario, de modifier le HTML affiché ou d'agir sur le modèle 3D.

Les actions sont définies 
- dans le fichier [neurosmart_views.js](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/js/neurosmart_views.js) pour les commandes d'affichage multimédias (*)
- et dans [neurosmart_brain_view.js](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/js/neurosmart_brain_view.js) pour les commandes de visualisation 3D du cerveau.

(*) Dans  [neurosmart_views.js](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/js/neurosmart_views.js), on trouvera la liste des vues HTML de l'interface.
Deux vues acceptent des commandes de scénario : la vue du contenu HTML affiché, et celle des contrôles de navigation, en bas de l'interface.

Pour ajouter une action, il faut:

- Ajouter l'action à la documentation [ici](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/doc/faire-scenario.md) et [là](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/doc/scenario_cheatsheet.md).
- Selon la nature de l'action aller dans l'objet Javascript correspondant et mettre à jour la liste `availableCommands` correspondante
- Ajouter ensuite la fonction à l'objet Javascript en se basant sur les implémentations voisines

Les paramètres définis dans le scenario (autre qu'**action**) sont passés à la fonction sous la forme d'un objet avec ses différentes propriétés.

Au moment de l'exécution du scénario, le programme vérifie que la commande existe et a été déclarée dans une des listes `availableCommands`.
Une erreur est renvoyée si la commande n'est pas reconnue.

Neurosmart repose sur deux librairies Javascript : Jquery pour la gestion de l'affichage HTML, et ThreeJS pour l'affichage 3D.
Il est donc possible d'utiliser leurs nombreuses possibilités au sein des différentes implémentations.

Pour des besoins très spécifiques, qui ne concerneraient qu'un projet particulier, il est également possible de définir un plugin pour modifier la vue du contenu HTML.
Les méthodes de ces plugins reçoivent elles aussi les paramètres du scénario en entrée, mais doivent retourner le contenu à insérer dans la vue HTML.
Le fichier [neurosmart_views_extended.js](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/js/neurosmart_views_extended.js) propose un exemple incluant plusieurs fonctions.
Le fichier javascript du plugin doit être ajouté aux scripts liés en bas de la page HTML.

