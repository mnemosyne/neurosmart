# Déployer le scénario

## Travailler de manière indépendante

L'ensemble des fichiers du répertoire ./public constituent un site web, il suffit de les installer sur le serveur de son choix.

La web application est aussi facilement intégrable dans une page comme ici https://pixees.fr/neurosmart

    <iframe public="https://mnemosyne.gitlabpages.inria.fr/neurosmart/" width="100%" height="400"><a target="_blank" href="https://mnemosyne.gitlabpages.inria.fr/neurosmart/"></a>cliquer ici</iframe>

## Co-travailler avec l'équipe de développement

Le dépot git est configuré pour avoir directement le résultat disponible donc en faisant :

    git pull ; git commit -a -m '$message' ; git push

le résultat s'affiche sur https://mnemosyne.gitlabpages.inria.fr/neurosmart

* Note : les sections de la webapplication sont bien segmentées, donc un bug dans un scénario n'a pas de chance de créer un effet de bord néfaste.

