# Faire son scénario

Après avoir pu [télécharger les fichiers](./telecharger.md), [installer un nouveau scénario](./creer-scenario.md), vois comment _programmer son scénario_ avant de [tester son scénario](./tester-scenario.md).

## Prendre en main un fichier de scénario.

### Utiliser une syntaxe assouplie

Si on regarde un exemple de [fichier de scénario](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/scenarios/modele/scenario.wjson) on remarque qu'il est écrit dans une syntaxe [JSON](https://www.json.org/json-fr.html) _assouplie_ (notion de [weak syntax](./wjson.md)) qui permet de simplifier l'écriture, plus précisément:

* Pas besoin de quote pour les mots sans espace ou les textes sur une seule ligne et sans caractères spéciaux (comme ':', ',' ou '}')
* Possibilité de rentrer des chaînes sur plusieurs lignes

tandis que l'analyse est tolérante à l'absence de virgules ou autres éléments syntaxiques non indispensables. Ces fichiers sont d'extension __.json__ pour les distinguer de fichiers en syntaxe JSON stricte.

### Utiliser le makdown pour les textes

Les textes peuvent être enrichis de liens, et autres éléments de mise en page. Ces éléments sont entrés en syntaxe [markdown](https://fr.wikipedia.org/wiki/Markdown).

### Commencer par entrer les méta-données

Chaque scénario correspond à une ressource de médiation scientifique et il est donc essentiel de bien renseigner _title_, _description_, _objective_, _audience_, et _prerequisite_, à partir des indications fournis dans le [modèle de scénario](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/scenarios/modele/scenario.wjson)



### Comprendre la structure d'un scénario

Un scénario est un ensemble de séquences, chaque séquence étant une liste d'actions. Le scénario démarre par la 1ère séquence, qui se déroule, puis offre à un menu pour passer à la séquence suivante ou aller vers une autre séquence, comme schematisé ici :

    {
       *** méta-données du scénario ***
       sequences : {
         $identifiant-de-la-sequence : [
           { action : nom-de-l-action, *** paramètres *** },
           *** actions suivantes ***
           { action : showNav, content : [ next, *** autres identifiants de séquence *** }
         ],
         *** autres séquences ***
       }
     }

Pour faire un scénario il suffit donc de connaitre les actions possibles, comme détaillé maintenant.

## Description des actions possibles

L'affichage se fait dans deux fenêtres, l'une permet d'afficher des contenus multi-média et intercatifs, l'autre le modèle 3D du cerveau. On trouve la [liste des actions ici](./scenario_cheatsheet.md).

### Manipulation de la fenêtre d'affichage

#### Affichage de texte en syntaxe markdown

* __action__ : __showText__
* __text__ : le texte à afficher en syntaxe markdown.

On peut aussi ajouter l'attribut :

* __clear__ : __true__ si présent efface tous les éléments de visualisation avant d’afficher l’élément.
* __top__ : __true__ si présent déplace le contenu pour afficher le nouvel élément en haut de la fenêtre.

Dans le cas d'un usage plus poussé on peut ajouter :

* __class__ : (optionel) définit la classe CSS de la division qui contiendra le texte HTML pour permettre un affichage spécifique (par exemple une couleur de fond, ou une bordure, …).
* __id__ : l’id HTML de la &lt;div class=”neurosmart” id=”$id”>&lt;/div> concernée, par défaut la précédente division utilisée, sinon la première division class=”neurosmart” de la page. Ceci n'est utile que si il y a plusieurs fenêtres d'affichage autour de la fenêtre d'affichage du modèle 3D.

Il est possible d'insérer des hyperliens relatifs qui permettent de lancer l'équivalent des commandes du scenario.

Quelques exemples pour manipuler le modèle 3D ( #pointAt ) ou pour se déplacer dans le scenario ( #go ) : 

    #pointAt?object=cerebellum
    #hiliteArea?area=area1
    #go?label=cerveau


Il est possible de pointer vers un autre scénario avec la syntaxe

    [texte du lien](##identifiant-du-scenario)

où l'identifiant du scénario est le nom du répertoire où sont stocké ses éléments.

#### Affichage d'un média

* __action__ : __showMedia__
* __url__ : l'adresse internet du média (image, vidéo, son, ...).
* __type__ : le type de média ( "image", "video", "sound" ).

Il est possible de préciser la taille du média via les paramètres width et height.
On peut utiliser les attributs _clear_ et _top_.
 
 Dans le cas d'un usage plus poussé on peut ajouter les attributs _class_ ou _id_.

#### Passage à la séquence suivante

* __action__ : __showNav__
* __content__ : [ __next__, _$identifiant-d-une-autre-séquence_, ..]

On définit des boutons permettant d’aller à la page suivante _next_, précédente _prev_, au début _home_ ou ailleurs en indiquant l'identifiant la séquence (qui s'affichera comme un titre).

#### Attente avant passage à la suite

* __action__ : __wait__
* __delay__ : value

Définit un temps d'attente en seconde pour laisser le temps de lire ou manipuler le cerveau.

### Manipulation du modèle 3D de cerveau

#### Positionnement du cerveau

Cette commande effectue une rotation/zoom de la caméra autour de l’origine 3D

* __action__ : __setCamera__
* __x__ : angle de rotation sur l’axe x,
* __y__  : angle de rotation sur l’axe y,
* __z__  : angle de rotation sur l’axe z,
* __d__ : distance par rapport à l’origine du modèle (par défaut : 5).

#### Ajuste la couleur et l'opacité d'une région 3D 

* __action__ : __setColor__
* __object__ : l’identifiant de la région du cerveau
* __color__ : la couleur de la région, sous la forme d’un string formaté du type «#00FFFF»;
* __opacity__ : l’opacité entre 0.0 et 1.0 (si 0.0 l’objet n’est plus affiché).

#### Ajuste l'opacité d'une région 3D 

* __action__ : __setOpacity__
* __object__ : l’identifiant de la région du cerveau @TODO Ajouter ici la liste des identifiants pour le modèle par défaut;
* __opacity__ : l’opacité entre 0.0 et 1.0 (si 0.0 l’objet n’est plus affiché).

#### Met en valeur une région du cerveau en diminuant l'opacité des autres régions 3D 

* __action__ : __hilite__
* __object__ : l’identifiant de la région du cerveau;

#### Pointe la camera sur une région du cerveau

* __action__ : __pointAt__
* __object__ : l’identifiant de la région du cerveau;

#### Pointe la caméra et met en valeur une région du cerveau en diminuant l'opacité des autres régions 3D 

* __action__ : __hiliteAndPointAt__
* __object__ : l’identifiant de la région du cerveau;

#### Rétablissement de la couleur et l'opacité d'origine d'une région 3D 

* __action__ : __restoreColor__
* __object__ : l’identifiant de la région du cerveau;

#### Rétablissement de la couleur et l'opacité d'origine de toutes les régions 3D 

* __action__ : __restoreAllColors__


Les identifiants des __régions__ du cerveau sont les suivants :

    - cerebellum : Cervelet
    - spinal-cord : Moelle épinière
    - thalamus : Thalamus
    - limbic : Système limbique
    - hippo-left : Hippothalamus gauche
    - hippo-right : Hippothalamus droit
    - basa-ganglia-left : Amygdale gauche
    - basa-ganglia-right : Amygdale droit
    - hemisphere-left : Hemisphère gauche
    - hemisphere-right : Hemisphère droit
    - insular-left : Cortex insulaire gauche
    - insular-right : Cortex insulaire droit

Les identifiants des __zones du cortex__ sont les suivants :

    - cortex-post-occipital-v1 : Cortex Postérieur Occipital V1
    - cortex-post-occipital-v2 : Cortex Postérieur Occipital V2
    - cortex-post-occipital-v3 : Cortex Postérieur Occipital V3
    - cortex-frontal-medial : Cortex Frontal Medial
    - cortex-post-parietal-wernicke : Cortex Postérieur Pariétal Wernicke
    - cortex-post-parietal-mst7 : Cortex Postérieur Pariétal MST 7
    - cortex-post-parietal-cingulate-post : Cortex Postérieur Pariétal Cingulate postérieur
    - cortex-post-parietal-sensoriel : Cortex Postérieur Pariétal Sensoriel
    - cortex-post-parietal-gustatory : Cortex Postérieur Pariétal Gustatory
    - cortex-post-parietal-mt5 : Cortex Postérieur Pariétal MT 5
    - cortex-frontal-dorsolateral : Cortex Frontal Lateral Dorsolateral
    - cortex-frontal-moteur : Cortex Frontal Moteur
    - cortex-frontal-ventrolateral : Cortex Frontal Lateral Ventrolateral Broca
    - cortex-frontal-ant : Cortex Frontal Anterior
    - cortex-post-temporal-sup-auditif : Cortex Postérieur Temporal Supérieur - Cortex auditif
    - cortex-post-temporal-gyrus : Cortex Postérieur Temporal Supérieur - Gyrus, temporal medial
    - cortex-post-temporal-wernicke : Cortex Postérieur Temporal Supérieur Wernicke
    - cortex-post-temporal-inf : Cortex Postérieur Temporal Inférieur



Un [scénario de démonstration](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/scenarios/demo-options-scenario/scenario.wjson) met en pratique ces différentes commandes.


## Etendre les possibilités de scénario.

Il est possible de créer des fonctionnalités supplémentaires via un système d'extension.

Les extensions concernent le contenu HTML. Elles retournent le contenu HTML qui doit être inséré au moment de l'exécution.

Le fichier _[public/assets/js/neurosmart_views_extended.js](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/js/neurosmart_views_extended.js)_ contient plusieurs exemples (vidéo, quiz).

En pratique, il suffit d'ajouter des méthodes à l'objet NS.viewPlugins. Elles seront alors disponibles dans le scenario.
Tous les paramètres d'un noeud du scenario sont passés en paramètre à la méthode sous la forme d'un objet _options_.

    NS.viewPlugins.myCustomCommand = function( options ){ 
    
        var newHtmlContent = '';

        ... 
    
        return newHtmlContent;
    }


Il est possible de passer des commandes ("pointAt", "setColor", ...) à la vue 3D en générant un évènement Javascript :


        this.dispatchEvent(new CustomEvent("executeAction", {
            bubbles: true,
            cancelable: true,
            detail: {
                action: "pointAt",
                params: { object: "cerebellum" }
            }
        }));


L'application Neurosmart utilise JQuery. Il est possible d'utiliser, au sein des extensions, les possiblités de cette librairie Javascript.

On peut aussi ajouter ces propres actions à tous les niveau du système comme [expliqué ici](https://gitlab.inria.fr/mnemosyne/neurosmart/-/blob/master/public/assets/doc/addon.md).
