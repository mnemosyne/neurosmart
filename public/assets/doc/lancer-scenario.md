# Lancer un scénario

Après avoir pu [télécharger les fichiers](./telecharger.md) il suffit d'ouvrir le fichier 

   public/index.html

* Les identifiants des scénarios correspondent au nom de leur répertoire dans __public/scenarios__
* Les scénarios sont référencés dans le fichier __public/scenarios/playlist.json__

On peut automatiquement charger le premier scénario de la playlist avec l'option __load=true__

    public/index.html?load=true

On peut automatiquement charger un scénario en donnant le nom de son fichier descriptif comme ici :

    public/index.html?scenario=scenarios/exploration-visuelle/scenario.wjson
