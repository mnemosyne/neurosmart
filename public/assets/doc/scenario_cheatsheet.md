# Liste de commandes wjson pour animer le scenario

Pour une aide détaillé cliquer [ici](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/assets/doc/faire-scenario.md).

## Navigation
```json
{ "action" : "wait", "delay" : 4}
{ "action" : "showNav", "content" : ["home", "redo", "prev", "next"]}
```
Nnaviguer selon les titres des blocs definis dans le scenario:
```json
{ "action" : "showNav", "content" : ["Introduction", "Présentation", "En savoir plus"]} 
```
## Ajouter des medias
### du texte
```json
{ "action" : "showText", "clear": false, "text": "_Bien :)_ Reprenons calmement"}
```
### des images
```json
{ "action" : "showMedia", "clear":true, type: "image", "url" : "scenarios/example/media/screencast0.jpg", height:"60%" }
{ "action" : "showText", "clear": false, "text": "voici une image ![my_img](scenarios/example/media/screencast0.jpg)"}
```
### des videos
```json
{ "action" : "showMedia", "top":"false", "type" : "video", "url" : "scenarios/usual-suspect/media/cut.mp4" }
```
```json
{ action: clearHtmlContent}
```

## Animer le cerveau
### bouger le modèle pour montrer une region (selon coordones du brain.json)
```json
{action : pointAt, object: thalamus}
```
### colorer une region
```
{action : hilite, object: thalamus}
{action: hiliteArea, area: thalamus}
```
### colorer une region et bouger le modèle pour la montrer
```
{ action : "hiliteAndPointAtArea", "area" : "cortex-post-temporal-inf"}
```
### remetre à zero
```
{action: restoreAllAreas}
```
#### Changer les couleurs
```json
{action : setOpacity, object: thalamus, value: 1.0}

{ "action" : "setColor", "object": "thalamus", "value": "#FFFFFF", colorPercent: 60%, opacity: 1.0}

{action : restoreColor, object: thalamus}

{action : restoreAllColors}
```
#### Changer la position
```json
{action : setCamera, x: 0, y: 90, z: 0, d: 0}
```

## Quiz
```json
{ action : showQuizzProposition, correct:true, text : "Le cerbellum", answer:"Bonne réponse"},
{ action : showQuizzProposition, correct:false, text : "Le thalamus", answer:"Non. Il s'agissait du cerebellum. Le thalamus se situe [ici](#hiliteAndPointAt?object=thalamus)"},
{ action : addScriptsToQuizzPropositions }
```
Remarquez l'example de lien vers une action sur le modèle de cerveau
