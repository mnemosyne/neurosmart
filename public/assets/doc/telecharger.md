# Télécharger les fichiers

Il est proposé de co-développer ensemble et partager à la fois les scénarios, les modèles 3D de cerveau et les mécanismes logiciels qui permettent de faire fonctionner cette ressource. On peut très simplement [télécharger les fichiers](https://gitlab.inria.fr/mnemosyne/neurosmart/-/archive/master/neurosmart-master.zip) et travailler sur son ordinateur, mais il est aussi possible de profiter des mécanismes de sauvegarde et de gestion de version de [GIT](https://fr.wikipedia.org/wiki/Git).

Cela permet d'[installer un nouveau scénario](./creer-scenario.md), de [programmer son scénario](./faire-scenario.md) et de [tester son scénario](./tester-scenario.md).

## Travailler de manière indépendante

### Récupérer les fichiers et créer une branche personnelle

__-1-__ Télécharger les fichiers avec la commande

    git clone https://gitlab.inria.fr/mnemosyne/neurosmart.git

ou via une interface graphique:

![Vue de l'interface](./img/git-clone.png)

__-2-__ Créer une nouvelle branche avec la commande

    git branch $nom-de-votre-branche

ce qui permettra de fusionner nos travaux ensuite

## Co-travailler avec l'équipe de développement

Il suffit demander à [rejoindre le projet](mailto:mecsci-accueil@inria.fr?subject=Je%20souhaite%20rejoindre%20le%20projet%20neurosmart&body=Bonjour,%0A%0C%20-%20Voici%20qui%20je%20suis%20:%20%0A%0C%20-%20Voici%20pourquoi%20je%20souhaite%20me%20joindre%20à%20vous%20:%20%0A%0CBien%20Cordialement.), nous vous fournirons toutes les informations.

__-1-__ Présentez-vous et expliquer votre motivation à participer au projet dans le mail.

__-2-__ Nous vous [ouvrirons](https://gitlab.inria.fr/siteadmin/doc/-/wikis/home#gitlab-accounts) un compte externe, puis vous inscrirons sur le [projet](https://gitlab.inria.fr/mnemosyne/neurosmart) comme "développeur·e"

__-3-__ Vous pourrez alors télécharger les fichiers avec la commande

    git clone git@gitlab.inria.fr/mnemosyne/neurosmart.git

ou via une interface graphique, et créer une nouvelle branche comme expliqué ci-dessus
