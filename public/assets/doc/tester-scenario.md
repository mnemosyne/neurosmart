# Tester son scénario

Après avoir pu [télécharger les fichiers](./telecharger.md), [installer un nouveau scénario](./creer-scenario.md), et [programmer son scénario](./faire-scenario.md) voici comment _tester son scénario_.

## Configuration préalable du navigateur

Lors du test on travaille en local sur des fichiers ce qui n'esr récemment pas autorisé par défaut pour des raisons de sécurité, voici comment configurer ou utiliser le navigateur

#### Pour firefox

Voici la [procédure](https://support.mozilla.org/en-US/questions/1264280):

* (1) Dans un nouvel onglet, entrer _about:config_ dans la barre d'adresse et appuyez sur Entrée / Retour. Cliquez sur le bouton promettant d'être prudent ou d'accepter le risque.
* (2) Dans la zone de recherche au-dessus de la liste, taper _uniq_ et attendre la liste proposée.
* (3) Double-cliquer sur la préférence _privacy.file_unique_origin_ pour changer la valeur de true à false.
* (4) Ouvrir la page `./public/index.html` dans le répertoire du projet.

Après le test, restaurez la configuration de sauvegarde en faisant la manipulation inverse.

#### Pour chrome

* (1) Fermer toutes les fenêtres de chrome. 
* (2) Lancer une nouvelle instance dans le répertoire du projet avec l'option:

    google-chrome --allow-file-access-from-files ./public/index.html 


## Mise en place du test

Le test se fait en lançant le scénario en mode test par le menu de _./public/index.html_ ou en utilisant le lien

    ./public/index.html?scenario=scenarios/$identifant-du-scenario/scenario.wjson&__debug__=true

où _$identifant-du-scenario_ est par exemple _exploration-visuelle_ et en ouvrant la console javascript du navigateur qui va produire des messages au fut et à mesure du déroulement du scénario.

## Déroulement du test

L'interpréteur de scénario va :

1. Lire le fichier en weak-JSON et le convertir en structure de donnée
2. Véifier que le contenu de ce fichier correspond au [schéma attendu](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/assets/etc/schema.json)
3. Exécuter le scénario au fur et à mesure des clics de l'utilisateur et signalr toute anomalie (par exemple, l'absence d'u nfichier)

## Comment mener le test ?

1. Ouvrir son fichier _scenario.wjson_ dans son éditeur de texte usuel
2. Afficher scénario en mode test et le faire fonctionner
3. Quand quelque chose est à changer (corriger une erreur, revoir un élément) :
  * Faire la corrections dans le fichier _scenario.wjson_ et sauvegarder
  * Recharcher la page qui lance le scénario en mode test et le refaire fonctionner

## Quelques conseils supplémentaires

* On peut lancer un scénario à partir d'une séquence donnée, sans revenir au debut, par exemple dans le [scénario d'exploration visuelle](https://gitlab.inria.fr/mnemosyne/neurosmart/blob/master/public/scenarios/exploration-visuelle/scenario.wjson) on peut démarrer à partir de la séquence _le-thalamus_ en utilisant le lien 

        ./public/index.html?scenario=scenarios/exploration-visuelle/scenario.wjson&__where__=le-thalamus
