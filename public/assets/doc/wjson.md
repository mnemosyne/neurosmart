# Utiliser la syntaxe wJSON

 Nous voulons qu’une personne qui n’est pas familière avec la programmation informatique puisse spécifier facilement un scénario, pour cela il suffit de maîtriser la notion de séquence d’instruction et de paramètre.

## Comprendre la syntaxe par l'exemple

La syntaxe wJSON pour "weak JSON" par rapport à la [syntaxe JSON stricte](https://www.w3schools.com/js/js_json_intro.asp) permet de définir une structure de données sans trop se soucier des détails de syntaxe, par exemple:
```
   {
     prénom: Jean-Pierre
     patronyme: Pierrejean
     age: 107
     adresse: "314 rue du Pi, La Quadrature"
     ami·e·s: [ lui, elle, "l'autre aussi" ]
     imaginatif
     présentation: "Au clair de la lune
Moi j'écris un mot, 
Avec oui ta plume,
Sans être le Pierrot"
   }
```
où l'on voit une donnée structurée de type carte de visite, avec des noms de champs (ex: `patronyme` ou `age`) des valeurs textuelles ou numériques, une liste de valeurs, et une valeur textuelle sur plusieurs lignes). 

- Ici les données structurées (on parle de t-uple) sont mises entre accolades `{ nom: valeur .}` et les listes (ou séquences) entre crochets `[ valeur_1 valeur_2 ]` 
- On voit que le champ `imaginatif` n'a pas de valeur, cela signifie simplement qu'il a la valeur `true` (vraie). 
- On note que les chaines de caractères avec des espaces ou sur plusieurs lignes sont entre guillemets `"`.

Rien de plus. Cela suffit à définir toutes les données structurées les plus complexes.

## Intérêt de la démarche

Nous voulons que des personnes sans formation technique en informatique, mais comprenant les principes du codage de l'information puisse aisément entrer des spécification comme des données structurées ou des séquences d'instruction.

Après plusieurs essais, il s'avère que la représentation de donnée proposée par la syntaxe JSON se comprend très bien, reste très lisible et facile à écrire. Avec un "mais".

Le "mais" est qu'on perd vraiment du temps à cause des méta-caractères oubliés ou en trop, alors que cela ne nuit nullement à l'analyse syntaxique, On a donc développer un mécanisme de lecture et d'écriture dans une syntaxe "allégée" qui à l'usage facilite vraiment les choses.

Le risque est bien entendu que certaines erreurs (par exemple une accolade oubliée) conduise à une structure de donnée erronée, mais il est facile de le vérifier. Par exemple, le texte en sortie peut-être mise dans une syntaxe JSON stricte indentée et lisible par l'humain permettant de vérifier que l'entrée était bien formée. Ou encore, le texte peut-être remis en forme, indenté, pour faire bien apparaître la structure pour la contrôler.

## Fonctionnalités proposées

Nous ne faisons que proposer une syntaxe d'entrée plus tolérante aux structures de données [JSON](https://www.w3schools.com/js/js_json_intro.asp), qui permet

- d'utiliser soit `:` ou `=` entre le nom et la valeur,
- d'utiliser soit `,` ou `;` ou une espace comme séparateur entre les items,
- d'utiliser soit `"` soit `'` comme guillemet,
- de ne pas utiliservitez les guillemets pour les chaînes sans espace ou sans méta-caractère `: = ,;]}`
- accepter la chaîne avec des séparateurs de ligne (remplacé par la séquence `\\n`), gérer également les caratères d'espacement `\\b`, `\\r`, `\\t`, `\\f`,
 - définir la valeur `true` pour un nom sans valeur explicite,
 - lire les nombres de la forme `0x?????` comme des nombres hexadecimaux,
 - accepter les commentaires de fin de ligne commençant par `#` ou `//` et les ignorer

Cependant, les séquences de chaînes unicode `\\uXXXX` et la séquence d'échappement solidus `\/` ne sont pas gérées (c'est-à-dire simplement reflétées dans la valeur de la chaîne).

Une conséquence est qu'il n'y a pas d'erreur de syntaxe, car toutes les chaînes génèrent une structure JSON (c'est-à-dire la structure JSON correcte exacte ou la plus proche, la métrique implicite étant définie par l'algorithme d'analyse). 
