"use strict";

var NS = NS || {};

/**
 * Main view
 *
 * Javascript object controlling the 3D View
 *
 */

NS.BrainView = {

	/**
	 * Initialization
	 * - adds the ns-view CSS class to be retrievable by the main controler NS.Controler
	 * - establishes a relation between the DOM element of the HTML content view and the Javascript class NS.ContentView
	 */
	init: function () {
		$(this.domElement).addClass("ns-view");
		$(this.domElement).data("controler", this);
	},

	/**
	 * Lists all the available public commands of the Brain view
	 * @return [{array}] - An array of the supported commands
	 */
	availableCommands: function () {
		return [
			"setCamera",
			"setColor",
			"restoreColor",
			"pointAt",
			"hilite",
			"hiliteAndPointAt",
			"restoreAllColors",
			"setOpacity"
		];
	},

	/**
	 * Loads a JSON file describing the model and its properties :
	 * - generic ID of submodels
	 * - nale of submodels in the .obj file
	 * - rendering order of submodels
	 * - position of the camera when running the command "pointAt" with a submodel ID reference
	 * - ID of areas,
	 * - colors of each area in the colored-texture ( used by the filter by color through shader)
	 *
	 * Ex : models3D/default/brain.json
	 *
	 * @param {string} playlistUrl - URL of the JSON file describing the model
	 */

	loadModelDescription: function (brainJsonURL) {

		this._brainUrl = brainJsonURL ? brainJsonURL : "models3d/default/brain.json";

		var pathArray = this._brainUrl.split('/');
		pathArray.pop();

		this._brainFolder = pathArray.join('/') + '/';
		this._isLoaded = false;

		// Début du chargement du fichier JSON
		NS.Utilities.asyncLoadTextFile(this._brainUrl, this, this.loadModelDescriptionComplete, this.loadModelDescriptionError);
	},

	/**
	 * Callback error called when JSON model description file could not be loaded
	 * @param {string} notFoundSchemaUrl - URL of the file describing the JSON schema
	 */

	loadModelDescriptionError: function (notFoundUrl) {
		console.log("JSON for model description not found", notFoundUrl);
	},

	/**
	 * Callback event called when JSON model description is fully loaded
	 * @param {string} modelDescriptionJson - JSON file content
	 */

	loadModelDescriptionComplete: function (modelDescriptionJson) {

		var brainJson = JSON.parse(modelDescriptionJson);

		// Json config file defines an associative array :
		// . brain zone "id" ( for scenarii ) -> group name in 3D Model .obj file
		// . optional parameters : color, colorPercent, opacity
		this._objectsState = brainJson.objects;

		var i, n = this._objectsState.length;
		var objectIdToObjectIn3DModel = [], stateObj;
		for (i = 0; i < n; i++) {
			stateObj = this._objectsState[i];
			objectIdToObjectIn3DModel[stateObj.id] = stateObj;
		}
		this._sceneIdToObject = objectIdToObjectIn3DModel;

		var areasInfos = brainJson.areas;
		var areaIdToAreaInfos = [], areaObj;
		for (i = 0; i < areasInfos.length; i++)
		{
			areaObj = areasInfos[i];
			areaObj.filterNo = i + 1; // RQ : Le struct d'index 0 est réservé à la mise en couleur/opacité du sous-modèle complet
			areaIdToAreaInfos[areaObj.id] = areaObj;
		}

		this._areaIdToAreaInfos = areaIdToAreaInfos; // associative array
		this._areaInfos = areasInfos; // linear array

		// Tableau associatif des sous-modèles ( THREE.Mesh )
		this._sceneObjects = [];

		this.create3DScene();
		this.load3DModel(brainJson.model, brainJson.texture, brainJson.textureColor);
	},

	getAreaInfosFromId: function (areaId) {
		var areaParameters = this._areaIdToAreaInfos[areaId];
		if (areaParameters) {
			return areaParameters;
		}
		return false;
	},

	getUniformFilterColorValues: function( tintColor, tintPercentage, opacity ) {

		var structs = [];
		var blackColor = new THREE.Color( "#000000" );

		// Index 0 is for color and opacity when no filter color is active
		structs.push({
			isActive: true,
			filterColor : blackColor,
			tintColor : new THREE.Color( tintColor ),
			tintPercentage: tintPercentage,
			opacity: opacity
		});

		// On ajoute autant de Structures que de couleur de filtre (les areas du fichier brain.json)
		var areaObj, i, areasInfos = this._areaInfos;
		for (i = 0; i < areasInfos.length; i++)
		{
			areaObj = areasInfos[i];

			structs.push({
				isActive: false,
				filterColor : new THREE.Color( areaObj.filterColor ),
				tintColor : blackColor,
				tintPercentage: 0.0,
				opacity: 0.0
			});
		}

		return structs;
	},

	_logObjectColorsOnce: true,

	logObjectColorsOnce: function () {
		this._logObjectColorsOnce = false;
		this.logObjectColors();
	},

	logObjectColors: function () {
		if (! this._logObjectColorsOnce)
		{
			this._logObjectColorsOnce = true;

			var objectId;
			for (objectId in this._sceneIdToObject)
			{
				var object3D = this.get3DObjectFromId(objectId);
				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;

				console.log(objectId, materialShaderUniforms);
			}
		}
	},

	updateUniformFilterColor: function ( materialShaderUniforms, filterIndex, tintColor, tintPercentage, opacity) {

		// filterIndex = 0 : teinte du sous-modèles
		// filterIndex = 1-19: teinte d'une area via son filtre de couleur

		// Liste des structs FilterColor
		var filterColors = materialShaderUniforms.uFilterColors.value;

		// Valeurs du Struct du filtre de couleur
		var filterColor = filterColors[filterIndex];
		filterColor.isActive       = true;
		filterColor.tintColor      = new THREE.Color( tintColor );
		filterColor.tintPercentage = tintPercentage;
		filterColor.opacity        = opacity;
	},

	updateUniformFilterColorOpacity: function ( materialShaderUniforms, filterIndex, opacity) {

		// filterIndex = 0 : teinte du sous-modèles
		// filterIndex = 1-19: teinte d'une area via son filtre de couleur

		// Liste des structs FilterColor
		var filterColors = materialShaderUniforms.uFilterColors.value;

		// Valeurs du Struct du filtre de couleur
		var filterColor = filterColors[filterIndex];
		filterColor.isActive       = true;
		filterColor.opacity        = opacity;

	},

	isUniformTransparent: function ( materialShaderUniforms) {

		// Liste des structs FilterColor
		var filterColors = materialShaderUniforms.uFilterColors.value;

		var transparent = false;

		var i, n = filterColors.length, filterColor;
		for(i=0;i<n;i++) {
			filterColor = filterColors[i];
			if ( filterColor.isActive && (filterColor.opacity < 1)) {
				transparent = true;
				break;
			}
		}

		return transparent;
	},

	disableUniformFilterColor: function ( materialShaderUniforms, filterIndex, opacity) {

		// filterIndex = 0 : teinte du sous-modèles
		// filterIndex = 1-19: teinte d'une area via son filtre de couleur

		// Liste des structs FilterColor
		var filterColors = materialShaderUniforms.uFilterColors.value;

		// Valeurs du Struct du filtre de couleur
		var filterColor = filterColors[filterIndex];
		filterColor.isActive = false;

		if (opacity !== undefined) {
			filterColor.opacity = opacity;
		}
	},

	/**
	 * Returns a object with the parameter from the JSON model description file
	 * Logs an error if the subModelID doesn't exist
	 *
	 * @param {string} submodelID - generic ID of submodel of the brain model ( the "real" name int the obj file could be different )
	 * @return [{object}] - parameter from the JSON model description file
	 */
	get3DObjectFromId: function (submodelID) {

		// Parameters from brainJson :
		var objectParameters = this.get3DObjectParametersFromId(submodelID);
		if (objectParameters) {
			// Real name in 3D Model
			var objectName = objectParameters.name;
			if (objectName) {
				// 3D Object in 3D Model
				return this._sceneObjects[objectName];
			}
		}
		return false;
	},

	/**
	 * Returns default parameters of an subModel
	 */
	get3DObjectParametersFromId: function ( submodelID ) {
		return this._sceneIdToObject[submodelID];
	},

	/**
	 * Dispatches an event that, if captured by the controller, could be displayed in the log view
	 */
	logError: function (info, obj) {
		this.domElement.dispatchEvent(new CustomEvent("logError", {
			bubbles: true,
			cancelable: true,
			detail: {
				info: info,
				obj: obj
			}
		}));
	},

	/**
	 * Create ThreeJS entities, initializes the trackball control, and starts the rendering loop
	 */
	create3DScene: function () {

		this._scene = new THREE.Scene();
		this._renderer = new THREE.WebGLRenderer({alpha: true});

		this._camera = new THREE.PerspectiveCamera(30, 1, 0.1, 100);

		var ambientLight = new THREE.AmbientLight( 0xcccccc, 0.5 );
		this._scene.add( ambientLight );

		var pointLight = new THREE.PointLight( 0xffffff, 0.8 );
		this._camera.add( pointLight );

		this._scene.add(this._camera);

		this.origin = new THREE.Vector3(0, 0, 0);

		// Controls setup
		this._controls = new THREE.TrackballControls(this._camera, this.domElement);
		this._controls.rotateSpeed = 3.0;
		this._controls.zoomSpeed = 2;
		this._controls.panSpeed = 2;
		this._controls.noZoom = false;
		this._controls.noPan = true;
		this._controls.staticMoving = true;
		this._controls.maxDistance = 20;
		this._controls.minDistance = 2;

		/*
		this._controls = new THREE.OrbitControls(this._camera, this.domElement, 1, 10);
		this._controls.minDistance = 2;
		this._controls.maxDistance = 5;
		this._controls.enableZoom = true;
		*/

		var t = this;

		this._controls.addEventListener('change', function () {
			t.onChangeControls();
		}, false);

		this._camera.position.x = 0;
		this._camera.position.y = 0;
		this._camera.position.z = 5;
		this._camera.lookAt(this.origin);

		// On mémorise la position initiale
		this.cameraPos0 = this._camera.position.clone();
		this.cameraUp0 = this._camera.up.clone();
		this.cameraQ = new THREE.Quaternion();

		this._renderer.setSize(this.domElement.clientWidth, this.domElement.clientHeight);
		this._renderer.setClearColor(0x000000, 0);

		this.domElement.appendChild(this._renderer.domElement);

		// Event Handlers
		window.onresize = function () {
			t._renderer.setSize(t.domElement.clientWidth, t.domElement.clientHeight);
		};
	},

	startRendering:function() {

		var t = this;
		t._firstRender = false;

		// Animation loop
		function animate(time) {

			requestAnimationFrame(animate);

			t.logObjectColors();
			t._controls.update();
			t._renderer.render(t._scene, t._camera);

			if ( ! t._firstRender ) {
				t._firstRender = true;
				t.domElement.dispatchEvent(new CustomEvent("brainRenderComplete", {
					bubbles: true,
					cancelable: true
				}));
			}
		}

		requestAnimationFrame(animate);
	},

	/**
	 * Callback event called when the 3D model is manipulated on screen
	 * @param {object} e - event
	 */

	onChangeControls: function (e) {

		var camera = this._camera;
		if (camera) {
			var cameraQ = camera.quaternion;
			var cameraEuler = new THREE.Euler().setFromQuaternion(cameraQ, 'XYZ');
			var cameraDistance = camera.position.length();

			this.domElement.dispatchEvent(new CustomEvent("cameraChange", {
				bubbles: true,
				cancelable: true,
				detail: {
					x: cameraEuler.x,
					y: cameraEuler.y,
					z: cameraEuler.z,
					d: cameraDistance
				}
			}));
		}
	},

	/**
	 * Enabled or disables user interaction
	 * @param {bool} interactionEnabled - The interaction state.
	 */
	setInteractionEnabled: function (interactionEnabled) {
		this._controls.enabled = interactionEnabled;
	},

	/**
	 * Loads all the resources of the model ( model, texture, filter colored-texture)
	 * Uses the ThreeJS loading manager, and initializes the ThreeJS entities when files are loaded
	 *
	 * @param {string} modelObjURL - URL of the .obj 3D model
	 * @param {string} textureURL - URL of the image of the main texture
	 * @param {string} textureColorURL - URL of the image of the colored-texture, used to hilite "areas" of the submodels (through shader)
	 */

	load3DModel: function (modelObjURL, textureURL, textureColorURL) {

		var t = this;

		t._isLoaded = false;

		var manager = new THREE.LoadingManager();
		manager.onProgress = function (item, loaded, total) {
			// console.log( "onProgress", item, loaded, total );
		};

		var vertShader = document.getElementById('phongVertexShader').textContent;
		var coloredFragShader = document.getElementById('phongFragmentShader').innerHTML;

		var textureLoader = new THREE.TextureLoader();

		// Texture du cerveau
		var brainTexture = textureLoader.load(this._brainFolder + textureURL, function (texture){ });

		// Texture pour le filtre par couleur
		var brainFilterColorTexture = textureLoader.load(this._brainFolder + textureColorURL, function (texture){ });

		var defaultColor = new THREE.Color("#000000");
		var defaultColorPercent = 0.0;
		var defaultOpacity = 1.0;

		var diffuseColor = 0xffffff;
		var specularColor = 0xffffff;
		var shininessValue = 100.0;

		var coloredShaderMaterial = new THREE.ShaderMaterial({
			vertexShader: vertShader,
			fragmentShader: coloredFragShader,
			lights: true
		});

		// Calcul du pourcentage de chargement du modèle
		var onProgress = function (xhr) {
			if (xhr.lengthComputable) {
				var percent = Math.round(xhr.loaded / xhr.total * 100);
				if (percent === 100) {
					t._isLoaded = true;
					t.startRendering();
					t.domElement.dispatchEvent(new CustomEvent("brainLoadingComplete", {
						bubbles: true,
						cancelable: true
					}));
				} else {
					t._isLoaded = false;
					t.domElement.dispatchEvent(new CustomEvent("brainLoadingProgress", {
						bubbles: true,
						cancelable: true,
						detail: {
							percent: percent
						}
					}));
				}
			}
		};

		var onError = function (xhr) {
			console.log("Erreur import OBJ", xhr);
		};

		// CHARGEMENT du fichier .obj :
		var group = new THREE.Group();
		var loader = new THREE.OBJLoader(manager);
		var shaderClone, nodeState, nodeOpacity, nodeColor, nodeColorPercent, nodeRenderOrder;

		loader.load(this._brainFolder + modelObjURL, function (object) {

			// Normalisation des dimensions :
			// On veut placer le modèle dans une boîte entre -1 et 1

			// Dimensions du modèle 3D
			var boundingBox = new THREE.Box3().setFromObject(object);

			var maxDimensions = boundingBox.max;
			var minDimensions = boundingBox.min;
			var maxDimension = Math.max(maxDimensions.x - minDimensions.x,
				maxDimensions.y - minDimensions.y,
				maxDimensions.z - minDimensions.z);

			// Règle de trois pour avoir des dimensions entre -1 et +1 :
			var normScale = 2.0 / maxDimension;

			// Mise à l'échelle
			object.scale.set(normScale, normScale, normScale);

			// Création initiale des uniforms de chaque sous-modèle :
			// Les fonctions du scenario modifieront les valeurs de ces uniforms
			object.traverse(function (node) {

				if (node instanceof THREE.Mesh) {

					// Par défaut :
					nodeOpacity = defaultOpacity;
					nodeColor = defaultColor;
					nodeColorPercent = defaultColorPercent;
					nodeRenderOrder = 0;

					// On reprend les valeurs éventuelles précisées dans le brain.json
					nodeState = t.findNodeDefaultState(node.name);
					if (nodeState) {

						if (nodeState.color && nodeState.color.length === 7 && nodeState.color.indexOf("#") === 0) {
							nodeColor = new THREE.Color(nodeState.color);
							nodeColorPercent = 1.0;
						}

						if (nodeState.colorPercent) {
							nodeColorPercent = NS.Utilities.getFloatFromString(nodeState.colorPercent);
							nodeColorPercent = NS.Utilities.clamp(nodeColorPercent, 0, 1);
						}

						if (nodeState.opacity) {
							nodeOpacity = NS.Utilities.getFloatFromString(nodeState.opacity);
							nodeOpacity = NS.Utilities.clamp(nodeOpacity, 0, 1);
						}

						if (nodeState.renderOrder) {
							nodeRenderOrder = parseInt(nodeState.renderOrder);
						}
					}

					shaderClone = coloredShaderMaterial.clone();

					// On commence par prendre en compte les uniforms des shaders ThreeJS (Phong, ...)
					var shaderUniforms = THREE.UniformsUtils.merge([
						THREE.ShaderLib.phong.uniforms,
						{ diffuse: { value: new THREE.Color(diffuseColor) } },
						{ specular: { value: new THREE.Color(specularColor) } },
						{ shininess: { value: shininessValue} },
					]);

					// On ajoute les uniforms Neurosmart référencant les 2 textures (cerveau et filtre de couleur du cortex)
					shaderUniforms.uBrainTexture = {type: "t", value: brainTexture};
					shaderUniforms.uFilterColorTexture = {type: "t", value: brainFilterColorTexture};

					// On ajoute les uniforms Neurosmart de gestion des couleurs et opacités
					shaderUniforms.uFilterColors = { value: t.getUniformFilterColorValues( nodeColor, nodeColorPercent, nodeOpacity ) };

					shaderClone.uniforms = shaderUniforms;

					node.material = shaderClone;
					node.material.transparent = nodeOpacity < 1;
					node.renderOrder = nodeRenderOrder;

					if (node.name !== "default") {
						// console.log("node", node.name, node);
					}

					// On stocke les références aux groupes
					t._sceneObjects[node.name] = node;

				}
			});

			group.add(object);

		}, onProgress, onError);

		this._scene.add(group);
	},

	findNodeDefaultState: function (nodeName) {
		var objectsState = this._objectsState;
		var i, objectState, n = objectsState.length;
		for (i = 0; i < n; i++) {
			objectState = objectsState[i];
			if (objectState.name === nodeName) {
				return objectState;
			}
		}
	},

	/**
	 * Returns true if the 3D files (obj, textures) are fully loaded, false otherwise
	 * Updated by the ThreeJS loading manager
	 *
	 * @return {bool} - The state of the view
	 */
	isLoaded: function () {
		return this._isLoaded;
	},


	/**
	 * ---------------------------------------------
	 * Public functions ( accessible from scenario)
	 * ---------------------------------------------
	*/

	/**
	 * Moves the camera
	 * Called by the controller
	 *
	 * Options are :
	 * - x : Euler angle along X axis, in degrees
	 * - y : Euler angle along Y axis, in degrees
	 * - z : Euler angle along Z axis, in degrees
	 * - d: distance to the center of the brain 3D model (normalized in a -1/+1 box)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 * @return Tween - Reference to the animation TweenLite object
	 */
	setCamera: function (options) {
		return this.startCameraRotation (options.x, options.y, options.z, options.d);
	},


	/**
	 * Points the camera towards a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * The coordinates of the camera for this submodel are defined in the model description file :  brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 * @return Tween - Reference to the animation TweenLite object
	 */
	pointAt: function (options) {

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined)
		{
			if (this.get3DObjectFromId(scenarioObjectId)) {
				// Id d'un sous-modèle
				this.pointAtObject(options);
			} else if (this.getAreaInfosFromId(scenarioObjectId)) {
				// Id d'une zone du cortex ?
				this.pointAtArea(options);
			} else {
				this.logError("pointAt : l'élément " + scenarioObjectId + " n'est pas reconnu !");
			}

		} else {
			this.logError("pointAt : aucun élément n'a été passé en paramètre !");
		}
	},


	/**
	 * Sets the color and eventually the opacity a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - value or color : color value in hexadecimal format ( ex : #FFFFFF )
	 * - colorPercent : percentage of the color to mix with the color of the texture
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setColor: function (options) {

		if ((options.value === undefined) && (options.color === undefined)) {
			options.value = "#FF0000";
		}

		var colorHexa = options.value || options.color;
		if (colorHexa.charAt(0) != "#") {
			colorHexa = "#" + colorHexa;
		}

		options.value = options.color = colorHexa;

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined)
		{
			if (this.get3DObjectFromId(scenarioObjectId)) {
				// Id d'un sous-modèle
				this.setObjectColor(options);
			} else if (this.getAreaInfosFromId(scenarioObjectId)) {
				// Id d'une zon e du cortex
				this.setAreaColor(options);
			} else {
				this.logError("setColor : l'élément " + scenarioObjectId + " n'est pas reconnu !");
			}

		} else {
			this.logError("setColor : aucun élément n'a été passé en paramètre !");
		}
	},

	/**
	 * Sets the opacity a subModel of the 3D Model or cortex area
	 *
	 * Options are :
	 * - object : generic ID of the submodel or cortex area ID
	 * - value or opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setOpacity: function (options) {

		if ((options.value === undefined) && (options.opacity === undefined)) {
			this.logError("setOpacity : le paramètre opacity n'est pas précisé !");
		}

		var opacity = options.value || options.opacity;
		opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
		opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

		options.opacity = opacity;

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined)
		{
			if (this.get3DObjectFromId(scenarioObjectId)) {
				// Id d'un sous-modèle
				this.setObjectOpacity(options);
			} else if (this.getAreaInfosFromId(scenarioObjectId)) {
				// Id d'une zon e du cortex
				this.setAreaOpacity(options);
			} else {
				this.logError("setOpacity : l'élément " + scenarioObjectId + " n'est pas reconnu !");
			}

		} else {
			this.logError("setOpacity : aucun élément n'a été passé en paramètre !");
		}

	},

	/**
	 * Hilites a submodel by :
	 * - setting the opacity a subModel to 1
	 * - adding transparency and setting the opacity of others to 0.2 (by default)
	 *
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - opacity : opacity value for others ( float < 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hilite: function (options) {
		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined)
		{
			if (this.get3DObjectFromId(scenarioObjectId)) {
				// Id d'un sous-modèle
				this.hiliteObject(options);
			} else if (this.getAreaInfosFromId(scenarioObjectId)) {
				// Id d'une zone du cortex ?
				this.hiliteArea(options);
			} else {
				this.logError("hilite : l'élément " + scenarioObjectId + " n'est pas reconnu !");
			}

		} else {
			this.logError("hilite : aucun élément n'a été passé en paramètre ");
		}
	},

	/**
	 * Called by the controller
	 *
	 * Options are :
	 * - area : ID of the area
	 *
	 * The coordinates of the camera for this area are defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 * @return Tween - Reference to the animation TweenLite object
	 */
	hiliteAndPointAt: function (options) {
		this.hilite(options);
		return this.pointAt(options);
	},

	/**
	 * Reset color and the opacity of the submodel or cortex area
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel, ID of cortex area
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreColor: function (options) {

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined) {

			if (this.get3DObjectFromId(scenarioObjectId)) {
				// Id d'un sous-modèle
				this.restoreObjectColor(options);
			} else if (this.getAreaInfosFromId(scenarioObjectId)) {
				// Id d'une zone du cortex ?
				this.restoreAreaColor(options);
			} else {
				this.logError("restoreColor : l'élément " + scenarioObjectId + " n'est pas reconnu !");
			}

		} else {
			this.logError("restoreColor : aucun élément n'a été passé en paramètre !");
		}
	},

	/**
	 * Reset color and the opacity of the all objects
	 * Called by the controller
	 *
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreAllColors: function ( options ) {
		this.restoreAllObjectColors(options);
		this.restoreAllAreas(options);
	},


	/**
	 * ----------------------------------------
	 * Private methods on submodels ( objects )
	 * ----------------------------------------
	 */

	/**
	 * Points the camera towards a subModel of the 3D Model
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * The coordinates of the camera for this submodel are defined in the model description file :  brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 * @return Tween - Reference to the animation TweenLite object
	 */
	pointAtObject: function (options) {
		var scenarioObjectId = options.object;
		var object = this.get3DObjectFromId(scenarioObjectId);
		if (object) {
			// Paramètres de l'objet issus de brain.json
			var objectParameters = this.get3DObjectParametersFromId(scenarioObjectId);
			var object3D = this.get3DObjectFromId(scenarioObjectId);
			if (objectParameters && object3D) {
				return this.startCameraRotation(
					objectParameters.x,
					objectParameters.y,
					objectParameters.z,
					objectParameters.d
				);
			}
		}
	},

	/**
	 * Sets the color and eventually the opacity a subModel of the 3D Model
	 * Called by the controller
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - color : color value in hexadecimal format ( ex : #FFFFFF )
	 * - colorPercent : percentage of the color to mix with the color of the texture
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */

	setObjectColor: function (options) {

		var colorHexa = options.value || options.color;
		var newColor = new THREE.Color(colorHexa);

		var colorPercent = options.colorPercent;
		colorPercent = NS.Utilities.getFloatFromString(colorPercent, 1.0);
		colorPercent = NS.Utilities.clamp(colorPercent, 0.0, 1.0);

		var opacity = options.opacity;
		opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
		opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

		// Modèle 3D de la partie du cerveau à modifier:

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;
		if (scenarioObjectId !== undefined) {
			var object3D = this.get3DObjectFromId(scenarioObjectId);
			if (object3D) {
				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;
				materialShader.transparent = opacity < 1;

				// Mise à jour du Struct d'index 0 qui définit la mise en couleur du sous-modèle dans son ensemble
				this.updateUniformFilterColor( materialShaderUniforms, 0, newColor, colorPercent, opacity);
			}
		}
	},

	/**
	 * Sets the opacity a subModel of the 3D Model
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setObjectOpacity: function (options) {

		var opacity = options.opacity;
		opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
		opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

		// Id de la zone du cerveau
		var scenarioObjectId = options.object;

		// Modèle 3D de la partie du cerveau à modifier:
		var object3D = this.get3DObjectFromId(scenarioObjectId);
		if (object3D) {
			var materialShader = object3D.material;
			var materialShaderUniforms = materialShader.uniforms;

			materialShader.transparent = opacity < 1;

			// Mise à jour du Struct d'index 0 qui définit la mise en couleur du sous-modèle dans son ensemble
			this.updateUniformFilterColorOpacity( materialShaderUniforms, 0, opacity);
		}
	},

	/**
	 * Hilites a submodel by :
	 * - setting the opacity a subModel to 1
	 * - adding transparency and setting the opacity of others to 0.2 (by default)
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 * - opacity : opacty value for others (float < 1, default 0.2)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hiliteObject: function (options) {

		this.restoreAllColors();

		var scenarioObjectId = options.object;

		// Opacity for other submodels
		var opacityForOthers = options.opacity;
		opacityForOthers = NS.Utilities.getFloatFromString(opacityForOthers, 0.2);
		opacityForOthers = NS.Utilities.clamp(opacityForOthers, 0.0, 1.0);

		var objectId, opacity;
		for (objectId in this._sceneIdToObject) {

			// Modèle 3D de la partie du cerveau à modifier:
			var object3D = this.get3DObjectFromId(objectId);
			if (object3D)
			{
				// Opacité = 1 pour l'objet à mettre en valeur
				// Opacité réduite pour les autres
				opacity = ( objectId === scenarioObjectId ) ? 1 : opacityForOthers;

				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;
				materialShader.transparent = opacity < 1;

				// Mise à jour du Struct d'index 0 qui définit la mise en couleur du sous-modèle dans son ensemble
				this.updateUniformFilterColorOpacity( materialShaderUniforms, 0, opacity);
			}
		}
	},

	/**
	 * Reset color and the opacity of the submodel
	 *
	 * Options are :
	 * - object : generic ID of the submodel
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreObjectColor: function (options) {

		var scenarioObjectId = options.object;

		// Sous-modèle 3D
		var object3D = this.get3DObjectFromId(scenarioObjectId);

		// Paramètres de l'objet issus de brain.json
		var objectParameters = this.get3DObjectParametersFromId(scenarioObjectId);

		if (object3D && objectParameters) {

			var color = objectParameters.color, defaultColor, colorPercent, opacity;
			if (color) {
				defaultColor = new THREE.Color(color);
				colorPercent = NS.Utilities.getFloatFromString(objectParameters.colorPercent, 0.0);
				colorPercent = NS.Utilities.clamp(colorPercent, 0.0, 1.0);
			} else {
				defaultColor = new THREE.Color("#FFFFFF");
				colorPercent = 0;
			}

			opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
			opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

			var materialShader = object3D.material;
			var materialShaderUniforms = materialShader.uniforms;
			materialShader.transparent = opacity < 1;

			// Mise à jour du Struct d'index 0 qui définit la mise en couleur du sous-modèle dans son ensemble
			this.updateUniformFilterColor( materialShaderUniforms, 0, defaultColor, colorPercent, opacity);
		}
	},

	/**
	 * Reset color and the opacity of the all objects
	 *
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	restoreAllObjectColors: function ( options ) {
		var objectId;
		for (objectId in this._sceneIdToObject) {
			this.restoreColor({ object: objectId });
		}
	},


	/**
	 * ------------------------------------------
	 * Private methods on areas (part of cortex )
	 * ------------------------------------------
	 */


	/**
	 * Points the camera towards a colored area of the cortex
	 *
	 * Options are :
	 * - area : ID of the area
	 *
	 * The coordinates of the camera for this area are defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 * @return Tween - Reference to the animation TweenLite object
	 */
	pointAtArea: function (options) {

		var areaId = options.object;
		var areaInfos = this.getAreaInfosFromId(areaId);
		if (areaInfos) {
			// Paramètres de l'objet issus de brain.json
			return this.startCameraRotation(
				areaInfos.x,
				areaInfos.y,
				areaInfos.z,
				areaInfos.d
			);
		}
	},

	/**
	 * Actives the shader color filter to hilite an area of 3D submodels
	 *
	 * Options are :
	 * - area : ID of the area
	 * - color : color value in hexadecimal format ( ex : #FFFFFF )
	 * - colorPercent : percentage of the color to mix with the color of the texture
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * The color applied to the area is defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setAreaColor: function (options) {

		var areaId = options.object;
		var areaInfos = this.getAreaInfosFromId(areaId);

		if (areaInfos && areaInfos.filterNo)
		{
			// Index de la zone dans le tableau de Struct
			var areaFilterNo = areaInfos.filterNo;

			// Par défault
			var tintColor = new THREE.Color(0x000000);
			var tintPercent = 0;

			if (options.color !== undefined) {

				if (options.color.charAt(0) != "#") {
					options.color = "#" + options.color;
				}

				// Color applied by the shader to the area
				tintColor = new THREE.Color(options.color);
				tintPercent = options.colorPercent;
				tintPercent = NS.Utilities.getFloatFromString(tintPercent, 1.0);
				tintPercent = NS.Utilities.clamp(tintPercent, 0.0, 1.0);
			}

			var opacity = options.opacity;
			opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
			opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

			console.log("setAreaColor", options.color, opacity);

			// On doit activer ce filtre de couleur à tous les sous-modèles
			// qui ont des zones de filtre de couleur (i.e. le cortex)

			// RQ : Ces sous-modèles diffèrent des autres par le paramètre 'areas:true' dans le fichier brainJson

			var objectId;
			for (objectId in this._sceneIdToObject) {
				var object3D = this.get3DObjectFromId(objectId);
				if (object3D) {
					var objectParameters = this.get3DObjectParametersFromId(objectId);
					if (objectParameters.areas)
					{
						var materialShader = object3D.material;
						var materialShaderUniforms = materialShader.uniforms;

						// On doit rendre inactive la mise en couleur du modèle en son entier
						this.disableUniformFilterColor( materialShaderUniforms, 0);

						// Mise à jour du Struct correspondant à la région du cortex ( = à la couleur de filtre de cette zone )
						this.updateUniformFilterColor( materialShaderUniforms, areaFilterNo, tintColor, tintPercent, opacity);

						// Si un seul des filtres d'une des zones du sous-modèle est actif et semi-transparent, on passe le shader en transparent
						materialShader.transparent = this.isUniformTransparent(materialShaderUniforms);

						// console.log("ok", areaFilterNo, tintColor, tintPercent, opacity)
					}
				}
			}
		}
	},

	/**
	 * Sets the opacity a subModel of the cortex area
	 *
	 * Options are :
	 * - object : cortex area ID
	 * - opacity : opacity value ( float from 0 to 1)
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	setAreaOpacity: function (options) {

		var areaId = options.object;

		var areaInfos = this.getAreaInfosFromId(areaId);
		var areaFilterNo = areaInfos.filterNo;
		if (areaInfos && areaInfos.filterColor)
		{
			// Index de la zone dans le tableau de Struct
			var areaFilterNo = areaInfos.filterNo;

			// Couleur de la zone dans la texture de filtre par couleur
			var areaColor = new THREE.Color(areaInfos.filterColor);

			var opacity = options.opacity;
			opacity = NS.Utilities.getFloatFromString(opacity, 1.0);
			opacity = NS.Utilities.clamp(opacity, 0.0, 1.0);

			// On doit activer ce filtre de couleur à tous les sous-modèles
			// qui ont des zones de filtre de couleur (i.e. le cortex)

			// Ces sous-modèles diffèrent des autres par le paramètre 'areas:true' dans le fichier brainJson

			var objectId;
			for (objectId in this._sceneIdToObject)
			{
				var object3D = this.get3DObjectFromId(objectId);
				if (object3D) {
					var objectParameters = this.get3DObjectParametersFromId(objectId);
					if (objectParameters.areas)
					{
						// On ne traite que les modèles ayant des sous-parties ( cf parameter 'areas:true' in brainJson file)
						var materialShader = object3D.material;
						var materialShaderUniforms = materialShader.uniforms;

						// On doit rendre inactive la mise en couleur du modèle en son entier
						this.disableUniformFilterColor( materialShaderUniforms, 0);

						// Mise à jour du Struct correspondant à la région du cortex ( = à la couleur de filtre de cette zone )
						this.updateUniformFilterColorOpacity( materialShaderUniforms, areaFilterNo, opacity);

						// Si un seul des filtres d'une des zones du sous-modèle est actif et semi-transparent, on passe le shader en transparent
						materialShader.transparent = this.isUniformTransparent(materialShaderUniforms);
					}
				}
			}
		}
	},

	/**
	 * Actives the shader color filter to hilite an area of 3D submodels
	 *
	 * Options are :
	 * - area : ID of the area
	 * - opacity : opacity value for others (float < 1, default 0.2)
	 *
	 * The color applied to the area is defined in the model description file : brain.json
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */
	hiliteArea: function (options) {

		this.restoreAllColors();

		var areaId = options.object;
		var areaInfos = this.getAreaInfosFromId(areaId);
		if (areaInfos)
		{
			// Index de la zone dans le tableau de Struct
			var areaFilterNo = areaInfos.filterNo;

			// Opacity for other submodels
			var opacityForOthers = options.opacity;
			opacityForOthers = NS.Utilities.getFloatFromString(opacityForOthers, 0.2);
			opacityForOthers = NS.Utilities.clamp(opacityForOthers, 0.0, 1.0);

			var blackColor = new THREE.Color( "#000000" );

			var objectId;
			for (objectId in this._sceneIdToObject) {

				var object3D = this.get3DObjectFromId(objectId);
				if (object3D) {

					var materialShader = object3D.material;
					var materialShaderUniforms = materialShader.uniforms;

					var objectParameters = this.get3DObjectParametersFromId(objectId);
					if (objectParameters.areas)
					{
						// Sub-models with areas (cortex) :
						// Opacity will be applied to areas, not sub-model

						// On doit rendre inactive la mise en couleur du modèle en son entier
						this.disableUniformFilterColor( materialShaderUniforms, 0, opacityForOthers);

						// Mise à jour des Struct : tous à opacity réduite, sauf la zone demandée
						var filterColors = materialShaderUniforms.uFilterColors.value;
						var i, n = filterColors.length;
						for(i=1;i<n;i++) {
							this.updateUniformFilterColor( materialShaderUniforms, i, blackColor, 0, i == areaFilterNo ? 1.0 : opacityForOthers);
						}
					}
					else
					{
						// Sub-models without areas :

						// Mise à jour du Struct d'index 0 qui définit la mise en couleur du sous-modèle dans son ensemble
						this.updateUniformFilterColorOpacity( materialShaderUniforms, 0, opacityForOthers);
					}

					// Si un seul des filtres d'une des zones du sous-modèle est actif et semi-transparent, on passe le shader en transparent
					materialShader.transparent = this.isUniformTransparent(materialShaderUniforms);
				}
			}
		}
	},

	/**
	 * Reset color and the opacity of a subModel-part of the cortex area
	 *
	 * Options are :
	 * - object : cortex area ID
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */

	restoreAreaColor: function (options) {

		var areaId = options.object;

		var areaInfos = this.getAreaInfosFromId(areaId);
		var areaFilterNo = areaInfos.filterNo;
		if (areaInfos && areaInfos.filterColor)
		{
			// Index de la zone dans le tableau de Struct
			var areaFilterNo = areaInfos.filterNo;

			// On doit désactiver ce filtre de couleur pour tous les sous-modèles
			// qui ont des zones de filtre de couleur (i.e. le cortex)

			// Ces sous-modèles diffèrent des autres par le paramètre 'areas:true' dans le fichier brainJson

			var objectId;
			for (objectId in this._sceneIdToObject)
			{
				var object3D = this.get3DObjectFromId(objectId);
				if (object3D) {
					// On ne traite que les modèles ayant des sous-parties ( cf parameter 'areas:true' in brainJson file)
					var objectParameters = this.get3DObjectParametersFromId(objectId);
					if (objectParameters.areas)
					{
						var materialShader = object3D.material;
						var materialShaderUniforms = materialShader.uniforms;
						materialShader.transparent = false;

						// Mise à jour du Struct correspondant à la région du cortex ( = à la couleur de filtre de cette zone )
						this.disableUniformFilterColor( materialShaderUniforms, areaFilterNo);
					}
				}
			}
		}
	},

	/**
	 * Disables the shader color all filters used to hilite areas of 3D submodels
	 *
	 * No options are defined at the moment
	 *
	 * @param [object] options - parameters from the JSON scenario
	 */

	restoreAllAreas: function ( options ) {

		var blackColor = new THREE.Color( "#000000" );

		var objectId;
		for (objectId in this._sceneIdToObject) {

			var object3D = this.get3DObjectFromId(objectId);
			if (object3D) {

				var materialShader = object3D.material;
				var materialShaderUniforms = materialShader.uniforms;

				var objectParameters = this.get3DObjectParametersFromId(objectId);
				if (objectParameters.areas)
				{
					// Sub-models with areas (cortex) :
					// Opacity will be applied to areas, not sub-model

					materialShader.transparent = false;

					// On doit rendre active la mise en couleur du modèle en son entier
					this.updateUniformFilterColor( materialShaderUniforms, 0, blackColor, 0, 1);

					// Mise à jour des Struct : on suprime le filtre
					var filterColors = materialShaderUniforms.uFilterColors.value;
					var i, n = filterColors.length;
					for(i=1;i<n;i++) {
						this.disableUniformFilterColor( materialShaderUniforms, i);
					}
				}
			}
		}
	},


	/**
	 * --------------------------
	 * Private methods on camera
	 * --------------------------
	 */

	/**
	 * Reset the initial value of the quaternion associated with the camera
	 * Defines the starting position/rotation of the the next transition
	 */
	resetCameraRotation: function () {
	    this.cameraQ = this._camera.quaternion;
    },

	/**
	 * Starts a transition to move the camera
	 *
	 * @param [number] angleDegX - Euler angle along X axis, in degrees
	 * @param [number] angleDegY - Euler angle along Y axis, in degrees
	 * @param [number] angleDegZ - Euler angle along Z axis, in degrees
	 * @param [number] distance - distance to the center of the brain 3D model
	 *
	 * @return Tween - Reference to the animation TweenLite object
	 */
	startCameraRotation: function (angleDegX, angleDegY, angleDegZ, distance) {

		var degToRad = Math.PI / 180.0;

		var angleX = angleDegX * degToRad; // Angle de rotation sur l'axe des X (radians)
		var angleY = angleDegY * degToRad; // Angle de rotation sur l'axe des Y (radians)
		var angleZ = angleDegZ * degToRad; // Angle de rotation sur l'axe des Z (radians)

		// Position initiale : distance et quaternion de la rotation
		this.iniR = this._camera.position.length();
		this.iniQ = new THREE.Quaternion().copy(this.cameraQ);

		// Position finale : distance et quaternion de la rotation
		this.endR = distance;
		this.endQ = new THREE.Quaternion().setFromEuler(new THREE.Euler(angleX, angleY, angleZ, 'XYZ'));

		// Quaternion où on enregistre l'interpolation
		this.curQ = new THREE.Quaternion();

		// Position de la caméra
		this.vec3 = new THREE.Vector3();

		var t = this;
		t.animationParams = { percent: 0 };


		// RQ : A la fin (onComplete), on doit mémoriser le quaternion définissant la rotation courante de la caméra

		return TweenLite.to( t.animationParams, 1, {
			percent: 1,
			ease: Power2.easeOut,
			onUpdate: function(tween) { t.slerpCameraRotation( tween.progress() ) },
			onUpdateParams: [ '{self}' ],
			onComplete: t.resetCameraRotation,
			onCompleteScope: t
		});

	},

	/**
	 * Updates the transition moving the camera
	 *
	 * Computes a intermediary quaternion between the initial and the final ones
	 * Applies the quartenion-based coordinates to the camera, and points the camera to the center of the model
	 *
	 * @param [number] percent - percentage of the transition (from 0 to 1)
	 *
	 */
	slerpCameraRotation: function( percent ){

		// Animation de la rotation
		THREE.Quaternion.slerp(this.iniQ, this.endQ, this.curQ, percent);

		// On applique le quaternion à la position initiale de la caméra
		this.vec3.x = this.cameraPos0.x;
		this.vec3.y = this.cameraPos0.y;
		this.vec3.z = this.cameraPos0.z;
		this.vec3.applyQuaternion(this.curQ);

		// Animation de la distance
		var r = this.iniR + percent * ( this.endR - this.iniR );
		this.vec3.multiplyScalar( r / this.vec3.length() );

		this._camera.position.copy(this.vec3);
		this._camera.lookAt(this.origin);

		// On applique le quaternion à l'orientation de la caméra
		this.vec3 = this.cameraUp0.clone();
		this.vec3.applyQuaternion(this.curQ);
		this._camera.up.copy(this.vec3);
	}
};

