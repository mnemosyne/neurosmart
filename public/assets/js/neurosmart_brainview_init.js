"use strict";

var NS = NS || {};
var NS2017 = NS2017 || {};
	
(function() {

	var brainLoadingElement = document.getElementById("brain-loading-infos");
	brainLoadingElement.innerHTML = "Loading...";

	var brainViewElement = document.getElementById("brain-view");

	brainViewElement.addEventListener("brainLoadingProgress", function(e) {
		brainLoadingElement.innerHTML = "Loading... " + e.detail.percent + "%";
	}, false);

	brainViewElement.addEventListener("brainLoadingComplete", function() {
		brainLoadingElement.innerHTML = "Rendering...";
	}, false);

	brainViewElement.addEventListener("brainRenderComplete", function() {

		brainLoadingElement.innerHTML = "";
		brainLoadingElement.style.display = "none";

		var locationHash = window.location.hash.substr(1);
		if (locationHash !== "")
		{
			var urlParts = locationHash.split('#').join('').split('?');
			var action = urlParts[0];
			if (action.length > 0)
			{
				// Transforme les paramètres del'URL en objet (ex: a=1&b=2 est transformée en ( a:1, b:2 } )
				var actionParams = JSON.parse('{"' + decodeURI(urlParts[1]).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')

				this.dispatchEvent(new CustomEvent("executeAction", {
					bubbles: true,
					cancelable: true,
					detail: {
						action: action,
						params: actionParams
					}
				}));
			}
		}

	}, false);

	// 1. HTML MainView
	var mainView = Object.create(NS.MainView, {
			'domElement': { value: brainViewElement } ,
	});
	mainView.init();

 	// 2. Controler
    var playerControler = Object.create(NS.Controler, {
		'domElement' : { value: brainViewElement },
		'mainView'   : { value: mainView }
	});
	playerControler.init();

	// 3. Brain View :
	var brainView = Object.create(NS.BrainView, {
		'domElement'   : { value: document.getElementById("neurosmart-brain") }
	});
	brainView.init();
	brainView.loadModelDescription();

}());