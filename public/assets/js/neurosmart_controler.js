"use strict";

var NS = NS || {};


/**
 * Application controller
 *
 * Responsible for :
 * - sending message to views
 * - handling events from views
 * - publishing logs
 * - loading external files (schema, scenario)
 * - managing navigation in scenario
 */

NS.Controler = {

    /**
     * Initialization of events listeners
     * ( Events delegation pattern )
     */

	init: function() {

		var t = this;

		this.domElement.addEventListener("click", function(e) {
			var target = $(e.target);
			if (target.hasClass("navigation")) {
				t.navigationChange( target.data("ns-nav") );
			} else if (target.hasClass("time-navigation")) {
				t.sequenceTimeChange( target.data("ns-time-label") );
			} else if (target.hasClass("start-btn")) {
				t.startScenario();
			} else if (target.hasClass("neurosmart-fullscreen")) {
				t.toggleFullscreen();
			} else if (target.hasClass("neurosmart-debug-tab")) {
				t.toggleDebugTab(e);
			} else if (target.hasClass("neurosmart-debug-minimize")) {
				t.toggleDebugMinimize();
			} else if (target.hasClass("sequence-link")) {
				e.preventDefault();
				t.goIndex(parseInt(target.data('index')));
			}
		});

		this.domElement.addEventListener("brainLoadingComplete", function() {
			t.log("Loading 3D Model Complete");
			t.brainIsLoaded = true;
			t.displayStartBtnWhenReady();
		}, false);

		this.domElement.addEventListener("brainLoadingProgress", function(e) {
			t.displayLoadingProgress(e.detail.percent);
		}, false);

		this.domElement.addEventListener("logError", function(e) {
			t.logError(e.detail.info, e.detail.object);
		}, false);
		
		this.domElement.addEventListener("change", function(e) {
			var target = $(e.target);
			if (target.hasClass("neurosmart-scenario-selector")) {
				var selectedOption = $(this).find(":selected");
				var scenarioUrl = selectedOption.val();
				t.loadScenario( scenarioUrl );
			}
		}, false);

		this.domElement.addEventListener("cameraChange", function(e) {
			t.logCameraPosition(e.detail.x, e.detail.y, e.detail.z, e.detail.d);
		}, false);

		this.domElement.addEventListener("executeAction", function(e) {
			t.executeAction(e.detail.action, e.detail.params);
		}, false);

		if (this.debug) {
            $(this.domElement).addClass("debug-mode");
		}

	},

    /**
     * Tab Selection of the debug view (log or json)
     */

	toggleDebugTab: function(e) {
		var tab = $(e.target);
		var tabId = tab.data("tab");
		this.debugView.selectTab(tabId);
	},

    /**
     * Minification of the debug view (log or json)
     */

	toggleDebugMinimize: function() {
        this.debugView.toggleMinimize();
	},

    /**
     * Displays logs :
     * - in browser developper tools (through console.log)
     * - in debug view ( log tab )
     *
     * It displays informations only if the debug property is set to true
     *
     * @param {string} info - text description of the log event
     * @param {string} obj - object containing details about log event
     * @param {string} bgcolor - background color when displaying log event
     * @param {string} forceDebug - force the display the debug view even if the debug proprty is set to false
     * ( Ex : when a scenario can't be loaded, the error message is displayed )
     */

	log: function( info, obj, bgcolor, forceDebug ) {
		if (this.debug || forceDebug) {
			
			var infoBgColor = bgcolor === undefined ? '#fae534' : bgcolor;
			var infoColor = "000000";

			// Formatage de la console du navigateur :
			console.log("%c[NS] " + info, 'background: ' + infoBgColor + '; color: ' + infoColor);
			if (obj) {
				console.log(obj);
			}
			
			// Formatage de la fenêtre de debug DOM
            this.debugView.appendToLog(info, infoBgColor, infoColor);
		}
	},

    /**
     * Displays logs for non crucial messages
     *
     * @param {string} info - text description of the log event
     * @param {string} obj - object containing details about log event
     */

	logLight: function( info, obj ) {
		this.log(info, obj , '#FFFFF0');
	},

    /**
     * Displays logs for error messages
     *
     * @param {string} info - text description of the log event
     * @param {string} obj - object containing details about log event
     */

	logError: function( info, obj ) {
		this.log(info, obj , '#FFC5C5', true);
	},

    /**
     * Add a message and open the log tabView of the debug window
     *
     * @param {string} info - text description of the log event
     * @param {string} obj - object containing details about log event
     */

    logAndDisplayError: function( info, obj ) {
        this.logError(info, obj);
        this.debugView.displayLog();
        this.debugView.showLogTab();
        this.debugView.scrollLogToBottom();
    },

    /**
     * Displays the 3D camera position (x, y, z) in the debugView
     *
     * @param {float} eulerX - Euler angle, rotation along X axis, in degree
     * @param {float} eulerY - Euler angle, rotation along Y axis, in degree
     * @param {float} eulerZ - Euler angle, rotation along Z axis, in degree
     * @param {float} cameraDistance - distance from the camera to the center of the 3D world
     */

	logCameraPosition: function( eulerX, eulerY, eulerZ, cameraDistance ) {
        if (this.debug) {
        	this.debugView.updateCameraPosition( eulerX, eulerY, eulerZ, cameraDistance )
        }
	},

    /**
     * Loads a JSON scenario file
     *
     * @param {string} scenarioUrl - URL of the JSON file describing the scenario
     * @param {string} startAtKey - Label in the scenario to begin with (optional)
     */

	loadScenario: function(scenarioUrl, startAtKey ) {
		this.startAtKey = startAtKey;
		NS.Utilities.asyncLoadTextFile(scenarioUrl, this, this.loadScenarioComplete, this.loadScenarioError);
	},

    /**
     * Callback error when loading a JSON scenario file
     *
     * @param {string} notFoundScenarioUrl - URL of the JSON file describing the scenario
     */

	loadScenarioError: function( notFoundScenarioUrl ) {
		if (notFoundScenarioUrl !== undefined) {
			var urlSplitArray = notFoundScenarioUrl.split(".");

			var extension = urlSplitArray.pop();
			this.logAndDisplayError("Échec du chargement du fichier ." + extension + "<br/>Vérifiez le chemin.");

			if (extension !== "json") {
				// On essaye de charger le fichier avec l'extension .json
				this.log("Essai de chargement du .json ...");
				urlSplitArray.push("json");
				this.loadScenario(urlSplitArray.join("."));
			}
		}
	},

    /**
     * Callback event when JSON scenario file is fully loaded
     *
     * @param {string} json - JSON file content
     */

    loadScenarioComplete: function( json ) {
		
		this.scenarioIsLoaded = true;
		this.scenario = wJSON.parse(json); // JSON.parse(json);
		
		this.log("Loading Scenario Complete : " + this.scenario.title);
		
		//this.log("JSON :", this.scenario);
		
		this.title = this.scenario.title;
		this.description = this.scenario.description;
		
		this.start();
		
		if (this.debug) {
			
			// Identation :
			var scenarioHtml = JSON.stringify(this.scenario, null, 4); // 4-spaces indent
			
			// Styles ( cf https://stackoverflow.com/a/7220510/1205110 )
			scenarioHtml = scenarioHtml.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
			scenarioHtml = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
				var cls = 'number';
				if (/^"/.test(match)) {
					if (/:$/.test(match)) {
						cls = 'key';
					} else {
						cls = 'string';
					}
				} else if (/true|false/.test(match)) {
					cls = 'boolean';
				} else if (/null/.test(match)) {
					cls = 'null';
				}
				return '<span class="' + cls + '">' + match + '</span>';
			});

			this.debugView.appendToScenario( scenarioHtml );

			// Schéma
			this.loadSchema("assets/etc/schema.json");
		}
	},

    /**
     * Loads a JSON playlist file, containing a list of scenario URLs
     *
     * @param {string} playlistUrl - URL of the JSON file describing the playlist
     * @param {boolean} loadFirst - If true, loads the first scenario in the playlist
     */
    loadPlaylist: function(playlistUrl, loadFirst) {
        this.loadFirstPlaylistScenario = loadFirst;
        NS.Utilities.asyncLoadTextFile(playlistUrl, this, this.loadPlaylistComplete, this.loadPlaylistError);
    },

	/**
	 * Callback error called when JSON playlist file could not be loaded
	 *
	 * @param {string} notFoundPlaylistUrl - URL of the JSON file describing the playlist
	 */

	loadPlaylistError: function( notFoundPlaylistUrl ) {
		if (notFoundPlaylistUrl) {
			var urlSplitArray = notFoundPlaylistUrl.split(".");
			var extension = urlSplitArray.pop();
			this.logAndDisplayError("Échec du chargement du ." + extension + " url = " + notFoundPlaylistUrl);
		}
	},

	/**
	 * Callback event called when JSON playlist is fully loaded
	 *
	 * @param {string} json - JSON file content
	 */

	loadPlaylistComplete: function( playListJson ) {
		var playList = JSON.parse(playListJson);
		if (Array.isArray(playList)) {
			if (playList.length >0) {
				this.mainView.displayPlaylist(playList);
				if (this.loadFirstPlaylistScenario) {
					var scenarioUrl = playList[0].url;
					if (scenarioUrl) {
						this.loadScenario( scenarioUrl );
					}
				}
			}
		}
	},

	/**
	 * Loads a JSON schema file
	 *
	 * @param {string} schemaUrl - URL of the file describing the JSON schema
	 */

	loadSchema: function(schemaUrl) {
		NS.Utilities.asyncLoadTextFile(schemaUrl, this, this.loadSchemaComplete, this.loadSchemaError);
	},

	/**
	 * Callback error called when JSON schema file could not be loaded
	 *
	 * @param {string} notFoundSchemaUrl - URL of the file describing the JSON schema
	 */

	loadSchemaError: function( notFoundSchemaUrl ) {
		console.log("JSON schema not found", notFoundSchemaUrl);
	},

	/**
	 * Callback event called when JSON schema is fully loaded
	 *
	 * @param {string} schemaJson - JSON file content
	 */

	loadSchemaComplete: function( schemaJson ) {

		//
		return;

		var schema = JSON.parse(schemaJson);
		
		this.log("Loading Schema Complete : " + this.scenario.title);
		this.log("JSON schema :", schema);
		
		var ajv = new Ajv( {allErrors: true} ); // options can be passed, e.g. {allErrors: true}
		var validate = ajv.compile(schema);
		
		var valid = validate(this.scenario);
		if (!valid) {
			var i, n = validate.errors.length, errorDescription;
			this.log("JSON schema : " + n + " errors");
			if (n > 0) {
				for(i=0;i<n;i++) {
					errorDescription = validate.errors[i];
					this.logLight( JSON.stringify(errorDescription, null, 4));
				}
			} else {
				this.log("No JSON schema errors !");
			}
		} else {
			this.log("JSON schema validation success !");
		}
	},

	/**
	 * Starts reading the scenario and update the UI
	 * - updates home view with the scenario Title and description, and waits for the "start" button
	 * - or displays JSON view if asked via URL parameter
	 */

	start: function() {
		
		if (this.mainView.setJson) {
			// Vue JSON pour débuggage
			this.mainView.setJson(this.scenario);
		}
		else
		{
			// Vue principale
			this.mainView.setTitle(this.title);
			this.mainView.setDescription(this.description);

			this.sequences = this.scenario.sequences;

			// Ids des séquences ( Object.keys est ordonné )
			this.sequenceKeys = Object.keys(this.sequences);

			// Affichage de la liste des séquences
			var i, sequenceLink = '', sequenceKey, sequenceLinkCssClass;
			for(i=0;i<this.sequenceKeys.length;i++) {
				sequenceKey = this.sequenceKeys[i];
				sequenceLinkCssClass = 'sequence-menu-item ' + ( i === 0 ? ' current' : '' );
				sequenceLink += '<li class="' + sequenceLinkCssClass + '"><a data-index="' + i +'" class="sequence-link" href="#">' + sequenceKey + '</a></li>';
			}
			$('#neurosmart-sequences').html(sequenceLink);

			this.logLight("Liste des séquences :" + this.sequenceKeys.join(', ') );

			this.displayStartBtnWhenReady();
		}
		
	},

	/**
	 * Displays the "start" button in the home view if the JSON scenario file and the 3D files are loaded
	 */

	displayStartBtnWhenReady: function() {
		if (this.scenarioIsLoaded && this.brainIsLoaded ) {
			// Affichage du bouton Démarrer
			this.mainView.canStart();
		}
	},

	/**
	 * Displays the loading percentage of the 3D elements in the home view
	 *
	 * @param {number} percent - PErcentage of the loading of 3D files (computed by ThreeJS)
	 */

	displayLoadingProgress: function(percent) {
		// Affichage du % de chargement
		this.mainView.loadingProgress(percent);
	},

	/**
	 * Toggles the application between the normal view in the browser window
	 * and the fullscreen view
	 */

	toggleFullscreen: function() {
		if (!document.fullscreenElement &&    // alternative standard method
			!document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
			if (document.documentElement.requestFullscreen) {
				document.documentElement.requestFullscreen();
			} else if (document.documentElement.msRequestFullscreen) {
				document.documentElement.msRequestFullscreen();
			} else if (document.documentElement.mozRequestFullScreen) {
				document.documentElement.mozRequestFullScreen();
			} else if (document.documentElement.webkitRequestFullscreen) {
				document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
			$(".neurosmart").addClass("fullscreen");
		} else {
			if (document.exitFullscreen) {
			  document.exitFullscreen();
			} else if (document.msExitFullscreen) {
			  document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
			  document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
			  document.webkitExitFullscreen();
			}
			$(".neurosmart").removeClass("fullscreen");
		}
	},

	/**
	 * Starts the scenario if the JSON scenario file and the 3D files are loaded
	 */

	startScenarioWhenReady: function() {
		if (this.scenarioIsLoaded && this.brainIsLoaded ) {
			this.startScenario();
		}
	},

	/**
	 * Updates the currentSequenceKey property
	 *  and starts the first sequence of the scenario
	 *
	 * Optionally, if the URL includes a "where" parameter with a sequence keyID, the scenario starts at this sequence
	 */

	startScenario: function() {
		// Lancement de la première séquence
		this.currentSequenceKey = this.startAtKey ? this.startAtKey : this.getFirstSequenceKey();
		this.playCurrentSequence();
	},

	/**
	 * Plays the sequence of the scenario that the property currentSequenceKey contains.
	 *
	 * It :
	 * - clears the HTML view(s)
	 * - builds a list of asynchronous actions, and plays the first action
	 *
	 * Each action fires a function call in the associate views
	 */

	playCurrentSequence: function() {
		
		this.log( "Démarrage de la séquence " + this.currentSequenceKey );

		// Mise à jour de la liste des séquences dans l'interface
		var currentSequenceIndex = this.getCurrentSequenceKeyIndex(this.currentSequenceKey);
		var sequencesItems = $('#neurosmart-sequences li');
		sequencesItems.removeClass('current');
		sequencesItems.eq(currentSequenceIndex).addClass('current');

		// Liste des actions de la séquence
		var sequenceObj = this.getSequenceByKey( this.currentSequenceKey );
		
		if (sequenceObj === undefined) {
			this.logError( "La séquence '" + this.currentSequenceKey + "' n'existe pas !");
			return;
		}
		
		var sequenceActions = sequenceObj.concat(); // clone;
		
		var j, m = sequenceActions.length, actionParams, action, viewFunction, t = this;
		
		// Liste des vues
		var elements = $(".ns-view", this.domElement);
		
		var i, n = elements.length, element, viewControler, viewAvailableCommands;
		// this.log("Nombre de vues : ", n);
		
		// On efface les vues HTML
		for(i=0; i<n; i++)
		{
			element = elements[i];
			element.getAttribute("data-controler");
			viewControler = $(element).data("controler");
			viewAvailableCommands = viewControler.availableCommands();
			if (viewAvailableCommands.indexOf( "clearHtmlContent" ) !== -1) {
				viewControler.clearHtmlContent();
			}
		}

		// On construit une séquence d'actions asynchrones
		// Chaque fonction retirera l'action qu'il executera, et passera à la suivante la pile d'actions restantes


		var timePosition = 0, currentLabelNo = 0, currentLabel;

		var timelineLite = t.timelineLite = new TimelineLite();
		timelineLite.addLabel(currentLabel = 'label' + currentLabelNo, timePosition);

		for(j=0; j<m; j++) {

			actionParams = sequenceActions[j];
			action  = actionParams.action;

			switch (action) {

				case "wait":

					var delay = isNaN(actionParams.delay) ? 1 : parseInt(actionParams.delay);
					t.logLight( "Attente de " + delay + " " + (delay > 1 ? "secondes" : "seconde") + "  ...");

					// Used to add a button in the navigation bar to bypass the waiting delay
					timelineLite.call( t.executeAction, ["startWaiting", { label: 'label' + (currentLabelNo + 1) } ], t, currentLabel );

					timePosition += delay;
					currentLabelNo ++;
					currentLabel = 'label' + currentLabelNo;

					timelineLite.addLabel(currentLabel, timePosition);
					timelineLite.call( t.executeAction, ["stopWaiting"], t, currentLabel );
				break;

				default :

					// Laisse le temps pour la transition
					timePosition += 0.5;
					currentLabelNo ++;
					currentLabel = 'label' + currentLabelNo;
					timelineLite.addLabel(currentLabel, timePosition);

					timelineLite.call( t.executeAction, [action, actionParams], t, currentLabel );
			}
		}
		
		// Lancement de la pile d'actions
		t.logLight("Lancement de la pile d'actions");
		t.playSequence();
	},

	playSequence: function() {
		$(".play").removeClass('paused');
		if (this.timelineLite ) {
			this.timelineLite.play();
		}
	},

	pauseSequence: function() {
		$(".play").removeClass('paused').addClass('paused');
		if (this.timelineLite ) {
			this.timelineLite.pause();
		}
	},

	moveForwardToLabel: function(sequenceLabel) {
		if (this.timelineLite ) {
			this.timelineLite.seek(sequenceLabel);
		}
	},

	toggleSequence: function() {
		this.IsSequencePlaying() ? this.pauseSequence() : this.playSequence();
	},

	IsSequencePlaying: function() {
		if (! this.timelineLite ) return false;
		return ! this.timelineLite.paused();
	},


	//
	// Commandes
	//

	/**
	 * Send command (and its param object) to views
	 * @param {string} action - name of the action command
	 * @param {object} params - parameters of the command as an JSON-object. Ex : { a:1, b:2 }
	 */

	executeAction: function( action, actionParams ) {

		var t = this, commandFound = false;

		if (actionParams) {
			actionParams.isFirstSequence = this.currentSequenceKey === this.getFirstSequenceKey();
		}

		switch(action) {

			case "go":
				var sequenceKey = actionParams.label;
				var scenarioForKey = this.getSequenceByKey(sequenceKey);
				if (sequenceKey) {
					t.go(sequenceKey);
				} else {
					t.logError( "Le paramètre de la commande 'go' n'est pas valide.");
				}

				break;

			default :

				// Action des vues et des extensions :

				// Liste des vues
				var elements = $(".ns-view", this.domElement);

				// On recherche les vues qui acceptent la commande :
				var i, n = elements.length, element, viewControler, viewAvailableCommands, viewFunction;

				for(i=0; i<n; i++) {
					element = elements[i];
					element.getAttribute("data-controler");
					viewControler = $(element).data("controler");

					// Commandes des vues de base (content, navigation) :
					viewAvailableCommands = viewControler.availableCommands();
					if (viewAvailableCommands.indexOf(action) !== -1) {
						viewFunction = viewControler[action];
						if (typeof viewFunction === "function") {
							commandFound = true;
							t.logLight("Lancement de la commande '" + action + "' " + JSON.stringify(actionParams));
							viewFunction.call(viewControler, actionParams);
						}
					}
					else if (typeof(viewControler.findPluginAction) === 'function')
					{
						// Commandes provenant d'extensions :
						viewFunction = viewControler.findPluginAction(action);
						if (typeof viewFunction === "function"){
							commandFound = true;
							t.logLight( "Lancement du plugin '" + action + "' " + JSON.stringify(actionParams));
							viewFunction.call(viewControler, actionParams);
						}
					}
				}

				if (! commandFound) {
					t.logError( "La commande '" + action + "' n'existe pas !");
				}
		}
	},

	
	//
	// Séquences du scénario
	//

	/**
	 * Returns the scenario first sequence keyID
	 */

	getFirstSequenceKey: function() {
		if (this.sequenceKeys.length > 0) {
			return this.sequenceKeys[0];
		}
	},

	/**
	 * Returns the scenario next sequence keyID, based on the current sequence
	 */
	getNextSequenceKey: function() {
		if (this.currentSequenceKey) {
			var pos = this.sequenceKeys.indexOf(this.currentSequenceKey) + 1;
			if ( pos < this.sequenceKeys.length ) {
				return this.sequenceKeys[pos];
			}
		}
	},

	/**
	 * Returns the scenario previous sequence keyID, based on the current sequence
	 */
	getPreviousSequenceKey: function() {
		if (this.currentSequenceKey) {
			var pos = this.sequenceKeys.indexOf(this.currentSequenceKey) - 1;
			if ( pos >= 0 ) {
				return this.sequenceKeys[pos];
			}
		}
	},

	/**
	 * Returns sequence key position in the sequence keys array
	 */
	getCurrentSequenceKeyIndex: function() {
		return this.sequenceKeys.indexOf(this.currentSequenceKey);
	},

	/**
	 * Returns the scenario sequence JSON object from the keyID
	 * @param {string} key - keyID of the sequence
	 */
	getSequenceByKey: function(key) {
		return this.sequences[key];
	},
	
	
	//
	// Navigation
	//

	/**
	 *  Called by a navigation event (cf init),
	 *  It plays the current sequence of the scenario
	 *
	 *  - it accepts generic IDs : "first" (or "home"), "prev" (or "previous"), "next"
	 *  - it also accepts scenario sequence keyID
	 *
	 * @param {string} navigationId - generic IDs ("previous", "next", ...) or keyID of a sequence
	 */

	navigationChange: function( navigationId ) {
		switch(navigationId) {
			case "home":
			case "first":
				this.first();
				break;
			case "previous":
			case "prev":
				this.previous();
				break;
			case "next":
				this.next();
				break;
			case "redo":
				this.redo();
				break;
			case "play":
				this.toggleSequence();
				break;
			default:
				this.go( navigationId );
		}
	},

	/**
	 *  Called by a navigation event (cf init),
	 *  It moves forward in time to a label of the current sequence (see timelineLite sequence build)
	 *
	 *  - it accepts label
	 *
	 * @param {string} time label
	 */

	sequenceTimeChange: function( timeLabel ) {
		console.log("moveTimeForward", timeLabel);
		this.moveForwardToLabel(timeLabel)
	},

	/**
	 *  Plays the first sequence of the scenario
	 */
	first: function() {
		var sequenceKey = this.getFirstSequenceKey();
		if (sequenceKey) {
			this.currentSequenceKey = sequenceKey;
			this.playCurrentSequence();
		}
	},

	/**
	 *  Plays the previous sequence of the scenario
	 */
	previous: function( ) {
		var sequenceKey = this.getPreviousSequenceKey();
		if (sequenceKey) {
			this.currentSequenceKey = sequenceKey;
			this.playCurrentSequence();
		}
	},

	/**
	 *  Plays the next sequence of the scenario
	 */
	next: function() {
		var sequenceKey = this.getNextSequenceKey();
		if (sequenceKey) {
			this.currentSequenceKey = sequenceKey;
			this.playCurrentSequence();
		}
	},

	/**
	 *  Plays the current sequence of the scenario again
	 */
	redo: function() {
		this.playCurrentSequence();
	},

	/**
	 *  Plays the given sequence of the scenario
	 *
	 * @param {string} sequenceKey - keyID of the sequence in the JSON scenario
	 */

	go: function(sequenceKey) {
		if (sequenceKey) {
			this.currentSequenceKey = sequenceKey;
			this.playCurrentSequence();
		}
	},

	/**
	 *  Plays the given sequence of the scenario
	 *
	 * @param {int} sequenceKey - keyID of the sequence in the JSON scenario
	 */

	goIndex: function(sequenceIndex) {
		if ((sequenceIndex >= 0) && (sequenceIndex < this.sequenceKeys.length)) {
			this.currentSequenceKey = this.sequenceKeys[sequenceIndex];
			this.playCurrentSequence();
		}
	}

};
