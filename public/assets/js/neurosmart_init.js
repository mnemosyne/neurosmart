"use strict";

var NS = NS || {};
var NS2017 = NS2017 || {};
	
(function() {
	
	// Scenario :
	var scenarioURL = NS.Utilities.getUrlParameter('scenario');
	if (scenarioURL === undefined)
	{
		// Playlist :
		var playlistURL = NS.Utilities.getUrlParameter('playlist');
		if (playlistURL === undefined)
		{
			// Chargement de la playlist par défaut
			playlistURL = "scenarios/playlist.json";

			// Option de la playlist : lance le premier scenario de la playlist
			var playlistLoadFirstURL = NS.Utilities.getUrlParameter('load');

			// Par défaut :
			if (playlistLoadFirstURL === undefined) {
				playlistLoadFirstURL = true;
			}
		}
	}

	// Vue principale
	var view = NS.Utilities.getUrlParameter('view');
	
	// Parent element
	var playerElement = document.getElementById("neurosmart-player");
	
	// Début du scénario
	var startAtKey = NS.Utilities.getUrlParameter('where');

	
	// 1. HTML MainView
	var mainView;
	
	if (view === "json")
	{
		// 1. HTML JSON
		mainView = Object.create(NS.JSONView, {
			'domElement': { value: playerElement } ,
		});
	}
	else
	{
		mainView = Object.create(NS.MainView, {
			'domElement': { value: playerElement } ,
		});
	}
	
	mainView.init();

    // 2. DebugView
    var debugView = Object.create(NS.DebugView, {
        'domElement': { value: document.getElementById("neurosmart-debug") } ,
    });
    debugView.init();

	// 3. Controler
    var playerControler = Object.create(NS.Controler, {
		'domElement' : { value: playerElement },
		'mainView'   : { value: mainView },
		'debugView'  : { value: debugView },
		'debug'      : { value: NS.Utilities.getUrlParameter('debug') }
	});
	playerControler.init();
	playerControler.loadScenario( scenarioURL, startAtKey );
	playerControler.loadPlaylist( playlistURL, playlistLoadFirstURL );
	
	// 4. HTML ContentView
	var htmlContentView = Object.create(NS.ContentView, {
		'domElement': { value: document.getElementById("neurosmart-html-content") } ,
		'plugins'   : { value: NS.viewPlugins = {} }
	});
	htmlContentView.init();
	
	// 5. Brain View :
	var brainView = Object.create(NS.BrainView, {
		'domElement'   : { value: document.getElementById("neurosmart-brain") }
	});
	brainView.init();
	brainView.loadModelDescription();

	// 6. Navigation
	var navigationView = Object.create(NS.NavigationView, {
		'domElement': { value: document.getElementById("neurosmart-navigation") } ,
	});
	navigationView.init();

}());