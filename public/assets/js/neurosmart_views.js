"use strict";

var NS = NS || {};


/**
 * Main view
 *
 * Javascript object controlling the mainView
 *
 */

NS.MainView = {

	/**
	 * Initialization
	 */
	init: function(){
	},

	/**
	 * Displays the scenario title in the home View
	 *
	 * @param {string} title - Title of the scenario
	 */

	setTitle: function(title){
		$('.scenario-title', this.domElement).html(title);
	},

	/**
	 * Displays the scenario description in the home View
	 *
	 * @param {string} description - Description of the scenario
	 */

	setDescription: function(description){
		$('.scenario-description', this.domElement).html(description ? description : "");
	},

	/**
	 * Displays start button to play the first sequence
	 * Called when scenario and 3D files are loaded
	 */
	canStart: function(){
		$('.loading-progress', this.domElement).css("display", "none");
		$('.start-btn', this.domElement).addClass("ns-ready").on("click", function() {
			$('.neurosmart-intro', this.domElement).addClass("ns-hidden");
			$('.neurosmart-app', this.domElement).addClass("ns-visible");
			$('.back-home-link', this.domElement).addClass("ns-visible");
		});
	},

	/**
	 * Displays 3D files loading percentage
	 * @param {number} percent - Percentage of loading
	 */
	loadingProgress: function(percent){
		$('.loading-progress', this.domElement).html( percent + '%');
	},

	/**
	 * Displays a dropdown menu with all the scenarii of a playlist
	 * @param {array} playList - array of scenario descriptions
	 */
	displayPlaylist: function( playList ){
		var n = playList.length;
		if (n > 0) {
			var i, scenarioInfos;
			var selectHtml = '<div class="neurosmart-scenario-selector-parent">';
			selectHtml += '<select class="neurosmart-scenario-selector"><option value="">Sélectionnez un autre scénario</option>';
			for(i=0;i<n;i++) {
				scenarioInfos = playList[i];
				if (scenarioInfos.title && scenarioInfos.url) {
					selectHtml += '<option value="'+  scenarioInfos.url +'">' + scenarioInfos.title + '</option>';
				}
			}
			selectHtml += '</select></div>';
			$('.description-content-footer', this.domElement).html( selectHtml );
		}
	},
};


/**
 * JSON view
 *
 * Javascript object controlling the json view
 * - Displays the JSON scenario
 * - With indentation and color for parameters
 *
 * A JSON tab view exist also in the debug view (along with the logs view)
 */

NS.JSONView = {

	init: function(){
		$(this.domElement).addClass("json");
	},

	/**
	 * Displays JSON file content, indented
	 * @param {string} json - JSON scenario
	 */
	setJson: function( json ){
		$(this.domElement).html( '<pre>' + JSON.stringify(json, null, 4) + '</pre>');
	},
	
};

/**
 * Debug view
 *
 * Javascript object controlling the debug view
 * - Contains tabs to switch between JSON view and Logs view
 * - Displays a windows above the HTML content and 3D views
 */

NS.DebugView = {

	init: function(){
		this.logsElement = $(".neurosmart-debug-content.ns-logs");
		this.jsonElement = $(".neurosmart-debug-content.ns-json");
	},

	/**
	 * Active the Json tab of the debugView
	 */
    selectJsonTab: function(){
		this.selectTab("ns-json");
    },

	/**
	 * Active the Logs tab of the debugView
	 */
    selectLogTab: function(){
		this.selectTab("ns-logs");
    },

	/**
	 * Active a tab of the debugView
	 * @param {string} tabId - id of the tab, as indicated in the data-tab attribute
	 */
    selectTab: function(tabId){

		var tab = $(".neurosmart-debug-tab[data-tab=" + tabId + "]");
		var tabContent = $(".neurosmart-debug-content[data-tab=" + tabId + "]");

		$(".neurosmart-debug-tab").removeClass("active");
		tab.addClass("active");

		$(".neurosmart-debug-content").removeClass("active");
		tabContent.addClass("active");

		$(".neurosmart-debug").removeClass("minimized");
    },

	/**
	 * Minimize/Maximize the debugView
	 */
    toggleMinimize: function() {
        $(this.domElement).toggleClass("minimized");
    },

    displayLog: function() {
        $(this.domElement).css("display", "block");
    },

	/**
	 * Appends a log into the Log view, displayed with specific text color and background color
	 * @param {string} info - text of the log
	 * @param {string} infoBgColor - background color of the new log
	 * @param {string} infoColor - text color of the new log
	 */
    appendToLog: function(info, infoBgColor, infoColor) {
        var infoStyle = "line-height:2em;padding:3px;margin-bottom:2px;background-color:" + infoBgColor + ";color:" + infoColor;
        this.logsElement.append('<pre style="' + infoStyle + '">' + info + '</pre>');
    },

	/**
	 * Sets the scenario JSON description in the Json tab of the DebugView
	 * @param {string} scenarioHtml - description of the scenario in JSON format
	 */
    appendToScenario: function(scenarioHtml) {
        this.jsonElement.html('<pre>' + scenarioHtml + '</pre>');
        $(".neurosmart-debug").css("display", "block");
    },

	/**
	 * Simulate a click event to force the display of the Logs tab of the DebugView
	 */
    showLogTab: function() {
        $(".neurosmart-debug-tab.ns-logs").trigger("click");
    },

	/**
	 * Updates the scroll of the logView to display the last logs at the bottom
	 */
    scrollLogToBottom: function() {
        var scrollTop = this.logsElement[0].scrollHeight - this.logsElement.height();
        this.logsElement.animate({ scrollTop: scrollTop }, 1000);
    },

	/**
	 * Updates the value of the 3D camera position displayed at the top of the DebugView
	 * The angles are displayed in degrees
	 * The distance value should be considered as "relative" as the model is normalized in a -1/+1 box
	 *
	 * @param {number} eulerX - value of the camera rotation along X axis, in radians
	 * @param {number} eulerY - value of the camera rotation along Y axis, in radians
	 * @param {number} eulerZ - value of the camera rotation along Z axis, in radians
	 * @param {number} cameraDistance - distance between position of the the camera and the center of the 3D model
	 */
    updateCameraPosition: function(eulerX, eulerY, eulerZ, cameraDistance) {

        var radToDeg = 180.0 / Math.PI;
        var eulerDegX = Math.floor(eulerX * radToDeg);
        var eulerDegY = Math.floor(eulerY * radToDeg);
        var eulerDegZ = Math.floor(eulerZ * radToDeg);
        var distance = Math.round(cameraDistance);

        var s = 'camera: ';
        s += '<span class="vars">x:</span>' + eulerDegX + ', ';
        s += '<span class="vars">y:</span>' + eulerDegY + ', ';
        s += '<span class="vars">z:</span>' + eulerDegZ + ', ';
        s += '<span class="vars">d:</span>' + distance;

        $(".neurosmart-debug-camera").html(s);
    }
};


/**
 * HTML rendering view
 *
 * Javascript object controlling the html content view
 * - Displays and animates the content (text, image, audio, video) of the scenario
 */

NS.ContentView = {

	/**
	 * Initialization
	 * - adds the ns-view CSS class to be retrievable by the main controler NS.Controler
	 * - establishes a relation between the DOM element of the HTML content view and the Javascript class NS.ContentView
	 */
	init: function(){
		$(this.domElement).addClass("ns-view");
		$(this.domElement).data("controler", this);
	},

	/**
	 * Lists all the available commands of the HTML content view
	 */
	availableCommands: function(){
		return [
			"clearHtmlContent",
			"showText",
			"showMedia"
		];
	},

	/**
	 * Clears the HTML content view
	 */
	clearHtmlContent: function(){
		$(this.domElement).empty();
	},

	/**
	 * Returns the height of the HTML content
	 */
	getHtmlContentHeight: function(){
		var contentElement = $(this.domElement);
		var c = contentElement.children();
		var h = 0;
		if (c.length) {
			// Calcul de la hauteur du contenu
			c.each(function(){
				h += $(this).outerHeight(true);
			});
		}
		return Math.floor(h);
	},

	/**
	 * Adds HTML content to the view
	 *
	 * Options :
	 * - { top:true } : adjusts the scroll so that the new content is displayed at the top
	 * - { clear:true } : clears the actual content before adding new content
	 *
	 * @param {string} htmlContent - html content to add at the bottom of to the content DOM element
	 * @param {object} options - optional parameters for displaying new content. Ex : { top:true } or { clear:true } see above for details
	 */
	addHtmlContent: function( htmlContent, options ){

		var domElement = this.domElement;
		var contentElement = $(domElement);
		var allElements, lastElement, scrollTop, scrollAnimDuration = 1000, opacityAnimDuration = 250;
		
		if (options.top)
		{
			// Hauteur de la partie visible de la vue
			var viewHeight = contentElement.height();

			// On calcule la taille avant l'ajout du nouvel élément
			var previousContentHeight = this.getHtmlContentHeight();

			// On retire l'éventuel supplément d'espace nécessaire au scroll pour les petits éléments
			var lastElementMarginBottom = contentElement.children().css('margin-bottom');
			if (! isNaN(lastElementMarginBottom)) {
				previousContentHeight -= parseInt(lastElementMarginBottom);
			}

			// On ajoute le nouvel élément
			contentElement.append( htmlContent );

			// Hauteur du contenu après l'ajout du nouvel élément
			var contentHeight = this.getHtmlContentHeight();

			allElements = contentElement.children();
			lastElement = allElements.last();

			// Attributs optionnels de l'auteur du scénarios : class, id
			if (options.class) {
				lastElement.addClass( options.class );
			}
			if (options.id) {
				lastElement.attr("id", options.id);
			}

			contentElement.children().css('margin-bottom', 0).removeClass("last");

			var addedContentHeight = contentHeight - previousContentHeight;

			// On veut caler le nouveau contenu en haut :
			// Si le nouveau contenu est trop petit, il faut ajouter un margin pour pouvoir scroller suffisament
			var marginBottom;
			if ( addedContentHeight < viewHeight) {
				marginBottom = ( viewHeight - addedContentHeight ) ;
				scrollTop = previousContentHeight + marginBottom;
			} else {
				marginBottom = 0;
				scrollTop = previousContentHeight;
			}

			// Ajout d'un espace pour les petits éléments pour pouvoir les caler en haut via le scroll
			lastElement.css('margin-bottom', marginBottom + "px").css("opacity", 0).addClass("last");

			contentElement.stop(true, true).animate({ scrollTop : scrollTop }, scrollAnimDuration, function() {
				lastElement.stop(true, true).animate({ opacity:1 }, opacityAnimDuration);
			});
		}
		else
		{
			// On ajoute le nouvel élément
			
			if (options.clear) {
				this.clearHtmlContent();
			}
			
			// Scroll avant ajout
			var currentScrollTop = contentElement.scrollTop();

			contentElement.append( htmlContent );

			allElements = contentElement.children();
			lastElement = allElements.last();

			allElements.css('margin-bottom', 0).removeClass("last");

			// Attributs optionnels de l'auteur du scénarios : class, id
			if (options.class) {
				lastElement.addClass( options.class );
			}
			if (options.id) {
				lastElement.attr("id", options.id);
			}

			// Scroll après ajout du contenu
			scrollTop = contentElement[0].scrollHeight - contentElement.height();
			
			// On veut éviter que le scroll ne remonte quand on retire la marge éventuelle du précédent-dernier élement
			// et que l'on ajoute le nouvel élément :

			var diffScrollTop = currentScrollTop - scrollTop;
			if (diffScrollTop > 0) {
				lastElement.css('margin-bottom', diffScrollTop + "px");
				scrollTop += diffScrollTop;
				scrollAnimDuration = 0;
			} else {
				// Durée de l'animation proportionnelle au déplacement du scroll
				scrollAnimDuration = 200 + diffScrollTop / 2;
			}
			
			lastElement.css("opacity", 0).addClass("last");

			contentElement.stop(true, true).animate({ scrollTop : scrollTop }, scrollAnimDuration, function() {
				lastElement.stop(true, true).animate({ opacity:1 }, opacityAnimDuration);
			});
			
		}

		// On force les liens à s'ouvrir vers une nouvelle fenêtre
		$('a', contentElement).off().attr('target','_blank');

		// Lien vers autre scenario :
		$('a[href^="?"]', contentElement).off().attr('target','_self');

		// Commandes passées dans un lien :
		$('a[href^="#"]', contentElement).on('click', function(e) {
			e.preventDefault();

			var url = $(e.target).attr('href');
			var urlParts = url.split('#').join('').split('?');
			var action = urlParts[0], actionParams;

			if (urlParts.length > 1) {
				// Transforme la chaine de type URL en objet (ex: a=1&b=2 est transformée en ( a:1, b:2 } )
				actionParams = JSON.parse('{"' + decodeURI(urlParts[1]).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
			}

			domElement.dispatchEvent(new CustomEvent("executeAction", {
				bubbles: true,
				cancelable: true,
				detail: {
					action: action,
					params: actionParams
				}
			}));
		});
		
	},

	/**
	 * Looks for plugin for an action that doesn't belongs to the default available commands
	 * listed in the "availableCommands" method
	 *
	 * Returns :
	 * - a function if this action name has been added by a plugin
	 * - false if no action with this name has been added by a plugin
	 *
	 * @param {string} action - name of a method to executes if it exists in a content view plugin
	 */
	findPluginAction: function( action ){
		var pluginAction = this.plugins[action];
		if ( pluginAction && typeof(pluginAction) === 'function' )
		{
			return function(options)
			{
				var newHtmlContent = pluginAction(options);
				this.addHtmlContent(newHtmlContent, options);
			};
		}
		return false;
	},

	/**
	 * Displays Text content in the view
	 *
	 * @param {object} options - object of the JSON scenario ( with action:"showText" )
	 */
	showText: function( options ){
		if ( options) {
			if (options.text) {

				// Markdown -> HTML
                var newMarkDown = options.text;
		// Adds specific markdowm meta-commands
		{
		  // Converts ##anchor to scenario links
		  newMarkDown = newMarkDown.replace(new RegExp("\\[([^\\]]*)\\]\\(##([^\\)]*)\\)"), "[$1](?scenario=scenarios/$2/scenario.wjson)");
		}

                var newHtmlText = markdown.toHTML( newMarkDown );
						this.addHtmlContent('<div class="ns-text">' + newHtmlText + '</div>', options);
			}
		}
	},

	/**
	 * Displays Image, Audio or Video content in the view
	 *
	 * The type property determines the type of the media to display : video, audio, image
	 * By default the media is supposed to be an image in a web compatible format (jpeg, png, gif)
	 *
	 * @param {object} options - object of the JSON scenario ( with action:"showMedia" )
	 */
	showMedia: function( options ){
		if ( options && options.url ) {
		
			var newHtmlContent = "", i, n;
			switch(options.type) {
					
				case "video":
					var video_urls = Array.isArray( options.url ) ? options.url : [ options.url ];
					var w = ! options.width  ? "100%" : options.width;
					var h = ! options.height ? "auto" : options.height;
					var video_url, video_extension;
					if (video_urls.length > 0) {
						newHtmlContent += '<video width="'+ w +'" height="'+ h +'" controls autoplay>';
						n = video_urls.length;
						for(i=0; i<n; i++){
							video_url = video_urls[i];
							video_extension = video_url.split(".").pop();
							newHtmlContent += '<source src="'+ video_url +'" type="video/'+ video_extension +'">';
						}
						newHtmlContent += 'Votre navigateur ne peut pas afficher cette vidéo.';
						newHtmlContent += '</video>';
					}
					break;
					
				case "audio":
					var audio_urls = Array.isArray( options.url ) ? options.url : [ options.url ];
					var audio_url, audio_extension;
					if (audio_urls.length > 0) {
						newHtmlContent += '<audio controls autoplay>';
						n = audio_urls.length;
						for(i=0; i<n; i++){
							audio_url = audio_urls[i];
							audio_extension = audio_url.split(".").pop();
							newHtmlContent += '<source src="'+ audio_url +'" type="audio/'+ audio_extension +'">';
						}
						newHtmlContent += 'Votre navigateur ne peut pas jouer ce son.';
						newHtmlContent += '</audio>';
					}
					break;
					
				default:
					
					var styles = '';
					
					if (options.url) {
						styles += 'background-image:url(' + options.url + ');';
					}
					
					if (options.width) {
						styles += 'width:' + options.width + ';';
					}
					
					if (options.height) {
						styles += 'height:' + options.height + ';';
					}
					
					newHtmlContent = '<div class="ns-image" style="'+ styles +'"></div>';
					break;
			}

			this.addHtmlContent(newHtmlContent, options);
		}
	}	
};

/**
 * Navigation view
 *
 * Javascript object controlling the navigation view
 * - displays buttons for navigating throught the differents parts of the scenario
 */

NS.NavigationView = {

	/**
	 * Initialization
	 * - adds the ns-view CSS class to be retrievable by the main controler NS.Controler
	 * - establishes a relation between the DOM element of the HTML content view and the Javascript class NS.ContentView
	 */
	init: function(){
		// JQuery
		$(this.domElement).addClass("ns-view");
		$(this.domElement).data("controler", this);
	},

	/**
	 * Lists all the available commands of the HTML navigation view
	 */
	availableCommands: function(){
		return ["showNav", "clearHtmlContent", "startWaiting", "stopWaiting"];
	},

	/**
	 * Clears the HTML navigation view
	 */
	clearHtmlContent: function(){
		$(this.domElement).empty();
	},

	/**
	 * Displays buttons in the navigation view
	 *
	 * @param {object} options - object of the JSON scenario ( with action:"showNav" )
	 */
	showNav: function( options ){

		if ( options && options.content) {

			// Par défaut, on affiche le retour à l'accueil, le suivant et le précédent (sauf au tout début)
			if (! options.content || (options.content.length === 0)) {
				options.content = options.isFirstSequence ? ['home', 'next'] : ['home', 'prev', 'next'];
			}

			this.clearHtmlContent();
			
			var i, n = options.content.length;
			var htmlText = "", buttonName, buttonClass;
			
			for(i=0;i<n;i++) {
				buttonName = options.content[i];
				buttonClass = buttonName.split(' ').join('-');
				htmlText += '<button class="navigation ' + buttonClass + '" data-ns-nav="' + buttonName + '">' + buttonName + '</button>';
			}
			
			$(this.domElement).html( htmlText );
		}
	},
	startWaiting: function( options ){
		this.removeByPassWaitingButton();
		$(this.domElement).append('<button class="time-navigation" data-ns-time-label="'+ options.label +'" >Passer l’attente</button>');
	},
	stopWaiting: function( options ){
		this.removeByPassWaitingButton();
	},
	removeByPassWaitingButton() {
		$('button.time-navigation', this.domElement).remove();
	}
};
