"use strict";

var NS = NS || {};

/**
 * NS.viewPlugins
 *
 * This file proposes an HTML content view plugin example, adding missing functions to the default NS.ContentView object
 *
 * The NS.viewPlugins object is initialized when creating the Ns.ContentView object :
 * var htmlContentView = Object.create(NS.ContentView, {
 *    'domElement': { value: document.getElementById("neurosmart-html-content") } ,
 *    'plugins'   : { value: NS.viewPlugins = {} }
 * });
 *
 */


/**
 * Embeds a YouTube Video
 *
 * Usage in scenario :
 * With/without autoplay :
 *  { "action" : "showYouTube", "url": "http://www.youtube.com/watch?v=MzxRq7JWQ28?autoplay=1"}
 *  { "action" : "showYouTube", "url": "http://www.youtube.com/watch?v=MzxRq7JWQ28"}
 *  { "action" : "showYouTube", "url": "http://www.youtube.com/watch?v=MzxRq7JWQ28" width="640" height="480" }
 *
 * @param {object} options - object of the JSON scenario ( with action:"showYouTube" )
 */

NS.viewPlugins.showYouTube = function( options ){
	
	var newHtmlContent = "";
	
	var w = ! options.width  ? "100%" : options.width;
	var h = ! options.height ? "100%" : options.height;
	
	// Transformation de l'URL pour embed :
	// De : https://www.youtube.com/watch?v=MzxRq7JWQ28
	// Vers : https://www.youtube.com/embed/MzxRq7JWQ28
	
	var youtube_url = options.url;
	if (youtube_url && youtube_url.length > 0) {
		var s = "v=";
		var pos_v = youtube_url.indexOf(s);
		if (pos_v !== -1) {
			youtube_url = "https://www.youtube.com/embed/" + youtube_url.substr(pos_v + s.length);
			newHtmlContent += '<iframe width="'+ w +'" height="'+ h +'" src="'+ youtube_url +'" allowfullscreen >';
		}
	}

	return newHtmlContent;
};

/**
 * Embeds of a Vimeo Video
 *
 * Usage in scenario : 
 *
 * With/Without autoplay : 
 *  { "action" : "showVimeo", "url": "https://vimeo.com/156824682?autoplay=1"}
 *  { "action" : "showVimeo", "url": "https://vimeo.com/156824682"}
 *
 * @param {object} options - object of the JSON scenario ( with action:"showVimeo" )
 */

NS.viewPlugins.showVimeo = function( options ){
	
	var newHtmlContent = "";

	var w = ! options.width  ? "100%" : options.width;
	var h = ! options.height ? "100%" : options.height;
	
	// Transformation de l'URL pour embed :
	// De : https://vimeo.com/156824682
	// Vers : https://player.vimeo.com/video/156824682
		
	var vimeo_url = options.url;
	if (vimeo_url && vimeo_url.length > 0) {
		var s = "vimeo.com/";
		var pos_v = vimeo_url.indexOf("vimeo.com/");
		if (pos_v !== -1) {
			vimeo_url = "https://player.vimeo.com/video/" + vimeo_url.substr(pos_v + s.length);
			newHtmlContent += '<iframe width="'+ w +'" height="'+ h +'" src="'+ vimeo_url +'" allowfullscreen >';
		}
	}

	return newHtmlContent;
};



/**
 * Displays a Quizz proposition
 * By clicking on it, an answer is displayed
 *
 * Usage in scenario :
 *  { "action" : "showQuizzProposition", "correct":true, "text" : "Cerebellum", "answer":"correct !"},
 *
 * @param {object} options - object of the JSON scenario ( with action:"showQuizzProposition" )
 */

NS.viewPlugins.showQuizzProposition = function( options ){

	if ( options && options.text) {

		// Markdown -> HTML
		var propositionMarkDown = options.text;
		var propositionText = markdown.toHTML( propositionMarkDown );

		var answerMarkDown = options.answer;
		var answerText = markdown.toHTML( answerMarkDown );

		var propositionIsCorrect = options.correct;
		var answerBackgroundColor =  propositionIsCorrect ? "#00FF00" : "#FF0000";

		var propositionId = $(".ns-text.answer").length;
		var propositionKey = "proposition" + propositionId;
		var propositionStyle = 'display:none;border-left: 10px solid ' + answerBackgroundColor;

		var propositionHtml = '<div class="ns-text proposition" data-proposition="' + propositionKey + '">' + propositionText + '</div>';
		propositionHtml += '<div class="ns-text answer" data-proposition="' + propositionKey + '" style="'+ propositionStyle +' ">' + answerText + '</div>';

		return propositionHtml;
	}

	return newHtmlContent;
};

/**
 * Initializes scripts on Quizz propositions
 *
 * Usage in scenario :
 *  { "action" : "addScriptsToQuizzPropositions" },
 *
 * @param {object} options - object of the JSON scenario ( with action:"addScriptsToQuizzPropositions" )
 */

NS.viewPlugins.addScriptsToQuizzPropositions = function( options ){

	$(".proposition").on('click', function(){
		var propositionKey = $(this).data("proposition");
		$(this).siblings('.answer[data-proposition=' + propositionKey + ']' ).css("display", "block");
		$(".proposition").off('click');
	});

};
