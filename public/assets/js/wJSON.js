/** Implements the JSON weak-syntax reader and writer. */
var wJSON = {
  /** Parses a data structure from a JSON weak-syntax string.
   * @param {string} value The value given as a string, using weak [JSON](http://json.org) syntax.
   * - The weak-syntax, with respect to the strict [JSON-syntax](https://www.w3schools.com/js/js_json_intro.asp) allows to:
   *   - use either ':' or '=' between name and value
   *   - use either ',' or ';' as item separator
   *   - use either '"' or "'" as quote
   *   - avoid quotes for strings without space or any meta-char ':=,;]}'
   *   - accept string with '\\n' line separators (replaced by the "\\n" sequence), also manage \\b, \\r, \\t, \\f
   *   - set the value 'true' for name without explicit value
   *   - However: '\\uXXXX' unicode string sequences and the '\/' solidus escaped sequence are not managed (i.e., but simply mirrored in the string value)
   *
   * In other words the input syntax accepts (i) implicit quote <tt>"</tt> when string reduces to one word, (ii) flexible use of comma <tt>,</tt> when optional, (iii) string on several lines, (iv) considering <tt>true</tt> as implicit value, (v) appending as a raw string trailer chars if any.
   * One consequence is that there is no syntax error all strings map on a JSON structure (i.e., the exact or closest correct JSON structure, the implicit metric being defined by the parsing algorithm). On the reverse the string output is a human readable indented strict JSON syntax allowing to verify that the input was well-formed.
   * @return {object} The parsed data-structure.
   */
  parse : function(value) {
    var Reader = {
      string : value,
      index : 0,
      read : function() {
        var value = this.read_value();
        this.next_space();
        if(this.index < this.string.length)
          return { value : value, trailer : this.string.substr(this.index) };
        return value;
      },
      read_value : function() {
        this.next_space();
        switch(this.string[this.index]) {
        case '{':
          return this.read_tuple_value();
        case '[':
          return this.read_list_value();
        default:
          return this.string2value(this.read_word(true));
        }
      },
      read_tuple_value : function() {
        var value = {};
        this.index++;
        for(var index0 = -1; index0 != this.index;) {
          index0 = this.index
                   this.next_space();
          if(this.index >= this.string.length)
            return value;
          if(this.string[this.index] == '}') {
            this.index++;
            return value;
          }
          var name = this.read_word();
          if(name == '')
            return value;
          this.next_space();
          var item = true;
          if (this.read_punctuation([':', '=']))
	    item = this.read_value();
          value[name] = item;
          this.next_space();
          this.read_punctuation([',', ';']);
        }
      },
      read_list_value : function() {
        var value = [];
        this.index++;
        for(var index0 = -1; index0 != this.index;) {
          index0 = this.index
                   this.next_space();
          if(this.index >= this.string.length)
            return value;
          if(this.string[this.index] == ']') {
            this.index++;
            return value;
          }
          value.push(this.read_value());
          this.next_space();
          this.read_punctuation([',', ';']);
        }
      },
      read_punctuation : function(symbols) {
        var found = false;
        while(this.index < this.string.length) {
	  this.next_space();
	  if (!symbols.includes(this.string[this.index]))
	    break;
	  found = true;
	  this.index++;
	}
	return found;
      },
      read_word : function(line = false) {
        return this.string[this.index] == '"' || this.string[this.index] == '\'' ? this.read_quoted_word(this.string[this.index]) : this.read_nospace_word(line);
      },
      read_quoted_word : function(quote) {
        var word = "";
        for(this.index++; this.index < this.string.length && this.string[this.index] != quote; this.index++) {
          if((this.string[this.index] == '\\') && (this.index < this.string.length - 1)) {
            this.index++;
            switch(this.string[this.index]) {
            case '\'':
            case '"':
            case '\\':
            case '/':
              word += this.string[this.index];
              break;
            case 'n':
              word += "\n";
              break;
            case 'b':
              word += "\b";
              break;
            case 'r':
              word += "\r";
              break;
            case 't':
              word += "\t";
              break;
            case 'f':
              word += "\f";
              break;
            default:
              word += "\\";
              word += this.string[this.index];
            }
          } else
            word += this.string[this.index];
        }
        if(this.index < this.string.length)
          this.index++;
        return word;
      },
      read_nospace_word : function(line = false) {
        var i0;
        for(i0 = this.index; this.index < this.string.length && (line ? this.no_endofline(this.string[this.index]) : this.no_space(this.string[this.index])); this.index++) {}
        return this.string.substr(i0, this.index - i0).trim();
      },
      next_space : function() {
        for(; this.index < this.string.length && this.isspace(this.string[this.index]); this.index++);
      },
      string2value : function(string) {
        if(new RegExp("^(true|false)$").test(string))
          return string == "true";
        else if(new RegExp("^[-+]?[0-9]+$").test(string))
          return parseInt(string);
        else if(new RegExp("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$").test(string))
          return parseFloat(string);
        else
          return string;
      },
      no_space : function(c) {
        return new RegExp("[^\\s,;:=}\\]]").test(c);
      },
      no_endofline : function(c) {
        return new RegExp("[^\\n,;:=}\\]]").test(c);
      },
      isspace : function(c) {
        return new RegExp("\\s").test(c);
      }
    };
    return Reader.read();
  },
  /** Converts a data structure the a light weak JSON syntax with a minimal number of quotes.
   * @param {object} value The parsed data-structure.
   * @return {string} A 2D formated string view of the value.
   */
  stringify : function(value) {
    var Writer = {
       write : function(value) {
         return this.write_value(value) + "\n";
       },
       write_value : function(value) {
	 if(!(value instanceof Object)) {
	   return this.write_word(value);
	 } else if(value instanceof Array) {
	   this.itab++;
           var strings = [];
	   for(var label = 0; label < value.length; label++)
	     strings.push(this.write_value(value[label]));
	   return this.write_strings("[", "]", strings);
	 } else {
	   this.itab++;
           var strings = [];
	   for(var label in value)
	     strings.push(this.write_word(label) + ": " + this.write_value(value[label]));
	   return this.write_strings("{", "}", strings);
	 }
      },
      write_word : function(value) {
        var string = String(value);
	if (string == "" || new RegExp("[\\s,;:=}\\]]").test(string))
	  string = "\"" + string.replace(new RegExp("([\"\\\\])", "g"), "\\$1") + "\"";
	return string;
      },
      write_strings : function(start, stop, strings) {
	var string = start;
	for(var i in strings)
	  string += this.write_line() + strings[i];
	this.itab--;
	string = string + this.write_line() + stop;
	return string;
      },
      write_line : function() {
        var string = "\n";
	for (var i = 0; i < this.itab; i++)
	  string += "  ";
	return string;
      },
      itab : 0
    };
   return Writer.write(value);
  },
  /* Wraps the object functionality as a query service.
   * - Implements the following query:
   *   - `action=wjson2json&value=wjson-string` : 
   *     - Reformats a weak JSON syntax string to a normalized JSON format.
   *   - `action=wjson2wjson&value=wjson-string` : 
   *     - Reformats a weak JSON syntax string to a normalized weak JSON format.
   * @param {map} query The HTTP query as structured `map<string,string>` object.
   * @return {string} The query response, or "" if no response or any error.
   */
  wrap : function(query) {
    switch(query.action) {
      case "wjson2json": return JSON.stringify(wJSON.parse(query.value), null, 2);
      case "wjson2wjson": return wJSON.stringify(wJSON.parse(query.value));
    }
    return "";
  }
};
