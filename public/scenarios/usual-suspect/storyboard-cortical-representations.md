## S4 - Cortex visuel primaire
L'information apporté par le nerf optique part à l'arrière du cerveau, dans le lobe occipital,
ou se trouve le [cortex visuel primaire](https://fr.wikipedia.org/wiki/Cortex_visuel_primaire) (aussi appelé V1).

Cette aire du cerveau encode notre champs visuel d'une façon particulière.
D'abord, la scène est renversé: tête en bas et droite-gauche, comme schématisé dans la figure suivante.

![img](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Cortex_strie.svg/330px-Cortex_strie.svg.png)

Une autre particularité: le centre de la vision (la [fovéa](https://fr.wikipedia.org/wiki/Fov%C3%A9a))
est beaucoup plus représenté que la périphérie. C'est pour cela que pour observer tous les détails d'une scène,
on doit [faire des mouvement des yeux pour l'explorer](lien scenario exploration visuelle).

L'encodage fait par V1 sert tant pour l'identification d'éléments dans une scène
 --- comme le visage de notre suspect --- comme pour les localiser dans l'espace.
La localisation est plutôt traité par des aires dorsales, vers le haut du cerveau.
L'identification, elle est réalisé par des aires ventrales, et c'est cette habilité qui nous intéresse ici.

![img](scenarios/usual-suspect/media/vision-cnn-a.png)

**Action:** allumer v1, dorsal, ventral, avec des couleurs différentes. mettre les couleurs dans le texte

## S5 - Une representation hierarchique
Une fois l'information visuele arrivé dans le cortex visuel primaire, quoi en faire ?
Notre cerveau adopte la bonne vieille strategie "diviser pour mieux reigner" :
on y va par morceaux, et puis on intègre le tout.
Chaque aire visuele calcule des indices locaux sur une région de ce que l'on voit,
et ces indices alimentent les calculs des aires suivantes,
qui vont les rassembler pour couvrir de plus en plus de "territoire" sur le champs visuel.
<!--Les aires ventrales de la vision encodent des indices basiques de l'image qu'on voit,
et regroupent les indices calculés par les aires qui viennent avant.-->

![img](scenarios/usual-suspect/media/vision-cnn-c.png)

Tout d'abord, V1 es capable d'identifier des portions de contours dans une image,
sans qu'ils soient déjà associés à un objet.
À ce niveau, sur notre suspect, on ne "voit" que des informations très localisés
comme des bouts de lignes qui forment les yeux, la bouche et le visage, ou aussi la direction des poils de ses cheveux ou sa barbe.

Puis les aires V2 et V4 participent a la combinaison de ces indices pour retrouver
 des formes simples, et puis combiner ces formes pour arriver a détecter des motifs plus complexes.
À ce niveau, on identifie déjà des formes qui vont ressembler a des yeux, un nez, une bouche.

Finalement, en s'apuiant sur ces indices calculés par V4,
le cortex infero-temporelle est capable tout intégrer et identifier des visages,
y inclus le visage de notre suspect en particulière.
![img](https://interstices.info/wp-content/uploads/jalios/classificateur/cortex.jpg)
*add image representative of indices computed on a face pic*



**Action:** allumage V1 V2 V4, IT séparément avec des couleurs différentes


## S6 - Une vision artificielle ?

*comment on fast-brain to introduce that this fast vision is feed forward
and that we try to mimic it with artificial neural networks. the point being
they share representational characteristics*
<!--img /href=src/scenarios/usual-suspect/media/fast-brain.jpg-->


*add image of cnn visualizations*

## Pour en savoir plus:

[Le cerveau à tous les niveaux : la vision](https://lecerveau.mcgill.ca/flash/d/d_02/d_02_cr/d_02_cr_vis/d_02_cr_vis.html).
[Reconnaiter un animal: notre cerveau est plus rapide que nous](https://interstices.info/reconnaitre-un-animal-notre-cerveau-est-plus-rapide-que-nous/)
